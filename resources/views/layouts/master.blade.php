<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>@yield('title') | {{ config('app-settings.company') }}{{ config('app-settings.shortname') }}</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.5 -->
	<link rel="stylesheet" href="{{ url('assets/bootstrap/css/bootstrap.min.css') }}">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{ url('assets/bootstrap/css/font-awesome.min.css') }}">
	<!-- Theme style -->
	<link rel="stylesheet" href="{{ url('assets/dist/css/AdminLTE.min.css') }}">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="{{ url('assets/dist/css/skins/_all-skins.min.css') }}">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
		@yield('content')
</div><!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="{{ url('assets/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<script type="text/javascript">
	function open_tool()
	{
		$.ajax({
			url : '{{url('log/2')}}',
			type : 'get',
			success : function(response) {
				console.log(response);
			}
		});
		window.open('{{ url('/cashier/sale') }}','','left=0, top=0, width=1100, height=900, toolbar=0, scrollbars=0, status=0');
	}
	/*20 mins*/
	setTimeout("window.location.href='{{url("/lockscreen")}}';", parseFloat("{{config('app-settings.lockscreen')}}")*20000 );

</script>

@yield('script')

<!-- Bootstrap 3.3.5 -->
<script src="{{ url('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ url('assets/dist/js/app.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ url('assets/dist/js/demo.js') }}"></script>

</body>
</html>
