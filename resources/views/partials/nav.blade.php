<header class="main-header">

  <!-- Logo -->
  <a href="{{ url('/'.$role.'/dashboard') }}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>K</b>{{ config('app-settings.shortname') }}</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>{{ config('app-settings.company') }}</b>{{ config('app-settings.shortname') }}</span>
  </a>

  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="{{ url('assets/dist/img/'.$profile->img) }}" class="user-image" alt="User Image">
            <span class="hidden-xs">{{ $profile->name }}</span>
          </a>
          @include('partials.nav.profile',['profile'=>$profile])
        </li>
      </ul>
    </div>

  </nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{ url('assets/dist/img/'.$profile->img) }}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>{{ $profile->name }}</p>
        <a href="#"><i class="fa fa-circle text-success"></i> {{ ucfirst($profile->role) }}</a>
      </div>
    </div>
    <!-- search form -->
    <form action="{{url($role.'/findtrans')}}" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
      <li class="treeview {{ $active=='timeline' ? 'active' : '' }}">
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ url('/'.$role.'/dashboard') }}"><i class="fa fa-circle-o"></i> Timeline</a></li>
        </ul>
      </li>
      @if($role=='cashier')
        <li class="treeview {{ $active=='tool' ? 'active' : '' }}">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>My Tool</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="javascript:;" onclick="open_tool();"><i class="fa fa-circle-o text-red"></i> Sale</a></li>
          </ul>
        </li>
      @endif

      @if('owner' === $role||'manager' === $role)
      <li class="treeview {{ $active=='tool' ? 'active' : '' }}">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Tools Log</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('/'.$role.'/aquabyte/Daily/'.date('Y-m-d'))}}"><i class="fa fa-circle-o text-yellow"></i> Aquabyte</a></li>
            <li><a href="{{url('/'.$role.'/e-load/Daily/'.date('Y-m-d'))}}"><i class="fa fa-circle-o text-aqua"></i> E-load</a></li>
            <li><a href="{{url('/'.$role.'/hands-on/Daily/'.date('Y-m-d'))}}"><i class="fa fa-circle-o text-green"></i> Hands-on</a></li>
            <li><a href="{{url('/'.$role.'/service/Daily/'.date('Y-m-d'))}}"><i class="fa fa-circle-o"></i> Services</a></li>
            <li><a href="{{url('/'.$role.'/photocopy-xerox/Daily/'.date('Y-m-d'))}}"><i class="fa fa-circle-o text-red"></i> Photocopy</a></li>
          </ul>
        </li>

      <li class="treeview {{ $active=='sale' ? 'active' : '' }}">
        <a href="#">
          <i class="fa fa-pie-chart"></i>
          <span>Sales Report</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ url($role.'/manage-sale?n=sale&t=Daily&r='.date('Y-m-d')) }}"><i class="fa fa-circle-o text-red"></i> Daily</a></li>
          <li><a href="{{ url($role.'/manage-sale?n=sale&t=Monthly&r='.date('Y-m')) }}"><i class="fa fa-circle-o text-yellow"></i> Monthly</a></li>
          <li><a href="{{ url($role.'/manage-sale?n=sale&t=Yearly&r='.date('Y')) }}"><i class="fa fa-circle-o text-aqua"></i> Yearly</a></li>
          <li><a href="{{ url($role.'/manage-sale?n=sale&t=Overall&r=2') }}"><i class="fa fa-circle-o"></i> Overall</a></li>
        </ul>
      </li>

      <li class="treeview">
        <a href="{{ url($role.'/manage-product/') }}">
          <i class="fa fa-files-o"></i>
          <span>Manage Products</span>
          {!! $zero_item!=0?'<span class="label label-danger pull-right">'.$zero_item.'</span>':'' !!}
        </a>
      </li>

      @endif

      @if('cashier'!= $role)
        <li>
          <a href="{{ url($role.'/manage-user/') }}">
            <i class="fa fa-th"></i> <span>Manage Users</span>
          </a>
        </li>
      @endif

      <li class="treeview {{ $active=='edit-account'?'active':'' }}">
        <a href="#">
          <i class="fa fa-edit"></i> <span>My Account</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{url($role.'/account')}}"><i class="fa fa-circle-o"></i> Edit</a></li>
        </ul>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
