<!DOCTYPE html>
<html>
<head>
    <title>Cashier Tool</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/bootstrap.min.css') }}" type="text/css" >
    <style type="text/css">
        mark {
            background-color: #B5B733 !important;
        }
    </style>
</head>
<body>
<div class="container"><br><br>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="col-sm-3 col-xs-12">
                <div class="input-group">
                    <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-cog"></div> Cashier Tool</span>
                    <select class="tool-select form-control">
                        <option value="sa">Sale</option>
                        <option value="aq">Aquabyte</option>
                        <option value="el">E-Load</option>
                        <option value="ho">Hands-on</option>
                        <option value="px">Photocopy</option>
                        <option value="se">Service</option>
                    </select>
                </div>
            </div>
            <div class="pull-right">
                <span class="trans-label"></span>
                <span class="trans-code"></span>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="panel-body">
            <!-- SALE **************************** -->
            <div class="tools sale-tool">
                <div class="col-sm-5 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-search"></div></span>
                        <input data-remote-list="{{ url('/api/product') }}"
                               data-list-highlight="true"
                               data-list-value-completion="true"
                               class="item-search form-control" type="text" placeholder="Search Product [home]"/>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-list-alt"></div></span>
                        <input data-remote-list="{{ url('/api/itemprice') }}"
                               data-list-highlight="true"
                               data-list-value-completion="true"
                               class="item-inquire form-control" type="text" placeholder="Inquire Items"/>
                    </div>
                </div>
            </div>
            <!-- START OF AQUABYTE **************************** -->
            <div class="tools aquabyte-tool" style="display:none;">
                <div class="col-sm-2 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-star"></div> Gal/s</span>
                        <input type="text" class="form-control aqua-quantity" name="aqua-quantity" placeholder="0">
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-certificate"></div> Price</span>
                        <input type="text" class="form-control aqua-price" name="aqua-amount" placeholder="0.00">
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-grain"></div> Amt</span>
                        <input type="text" readonly class="form-control tool-amount aqua-amount" name="aqua-amount" placeholder="0.00">
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-thumbs-up"></div> Dscount</span>
                        <input type="text" class="form-control aqua-discount" name="aqua-discount" placeholder="0.00">
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12"><button class="btn btn-primary add-btn"><span class="glyphicon glyphicon-plus-sign"></span> Add</button></div>
            </div>
            <!-- END OF AQUABYTE **************************** -->

            <!-- START OF E-LOAD **************************** -->
            <div class="tools eload-tool" style="display:none;">
                <div class="col-sm-2 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-globe"></div> Ntwrk</span>
                        <input type="text" class="form-control el-network" name="el-network" placeholder="Globe/Smart">
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-phone"></div></span>
                        <input type="text" class="form-control el-number" name="el-number" placeholder="0926....">
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-star"></div> Load</span>
                        <input type="text" class="form-control el-load" name="el-load" placeholder="0">
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-grain"></div> Amt</span>
                        <input type="text" class="form-control tool-amount el-amount" name="el-amount" placeholder="0.00">
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-thumbs-up"></div> Dscount</span>
                        <input type="text" class="form-control el-discount" name="el-discount" placeholder="0.00">
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12"><button class="btn btn-primary add-btn"><span class="glyphicon glyphicon-plus-sign"></span> Add</button></div>
            </div>
            <!-- END OF E-LOAD **************************** -->

            <!-- START OF HANDS-ON **************************** -->
            <div class="tools hands-on-tool" style="display:none;">
                <div class="col-sm-2 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-star"></div> per hr</span>
                        <input type="text" class="form-control ho-perhour" name="ho-perhour" value="20">
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-dashboard"></div> In</span>
                        <input type="text" class="form-control ho-tin" name="ho-tin" placeholder="">
                        <input type="hidden" class="form-control ho-time" name="ho-time">
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-hourglass"></div> Out</span>
                        <input type="text" class="form-control ho-tout" name="ho-tout" placeholder="">
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-grain"></div> Amt</span>
                        <input type="text" class="form-control tool-amount ho-amount" name="ho-amount" placeholder="0.00">
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-thumbs-up"></div> Dscount</span>
                        <input type="text" class="form-control ho-discount" name="ho-discount" placeholder="0.00">
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12"><button class="btn btn-primary add-btn"><span class="glyphicon glyphicon-plus-sign"></span> Add</button></div>
            </div>
            <!-- END OF HANDS-ON **************************** -->

            <!-- START OF PHOTOCOPY **************************** -->
            <div class="tools px-tool" style="display:none;">
                <div class="col-sm-2 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-star"></div> Pc/s</span>
                        <input type="text" class="form-control px-quantity" name="px-quantity" placeholder="0">
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-certificate"></div> Price</span>
                        <input type="text" class="form-control px-price" name="px-price" placeholder="0.00">
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-grain"></div> Amt</span>
                        <input type="text" readonly class="form-control tool-amount px-amount" name="px-amount" placeholder="0.00">
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-thumbs-up"></div> Dscount</span>
                        <input type="text" class="form-control px-discount" name="px-discount" placeholder="0.00">
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12"><button class="btn btn-primary add-btn"><span class="glyphicon glyphicon-plus-sign"></span> Add</button></div>
            </div>
            <!-- END OF PHOTOCOPY **************************** -->

            <!-- START OF SERVICES **************************** -->
            <div class="tools se-tool" style="display:none;">
                <div class="col-sm-2 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-star"></div> Qty</span>
                        <input type="text" class="form-control se-quantity" name="se-quantity" placeholder="0">
                    </div>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-list"></div> Desc</span>
                        <input type="text" class="form-control se-desc" name="se-desc" placeholder="">
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-certificate"></div> Price</span>
                        <input type="text" class="form-control se-price" name="se-price" placeholder="">
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-grain"></div> Amt</span>
                        <input type="text" readonly class="form-control tool-amount se-amount" name="se-amount" placeholder="0.00">
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1"><div class="glyphicon glyphicon-thumbs-up"></div> Dscnt</span>
                        <input type="text" class="form-control se-discount" name="se-discount" placeholder="0.00">
                    </div>
                </div>
                <div class="col-sm-1 col-xs-12"><button class="btn btn-primary add-btn"><span class="glyphicon glyphicon-plus-sign"></span> Add</button></div>
            </div>
            <!-- END OF SERVICES **************************** -->

            <input type="text" name="barcode" class="barcode" placeholder="Barcode Reader" autofocus style="position:absolute;top:-100px">
            <div class="row no-margin-padding">
                <div class="col-sm-3 pull-right">
                    <span style="font-size:14pt;">Total : </span>
                    <span><strong class="total-amount" style="font-size:28pt; color: red;">0.00</strong></span><br>
                    <span style="font-size:14pt;">Item/s : <strong class="total-item">0</strong></span><hr>
                    <div class="input-group cash-wrap">
                        <span class="input-group-addon">Cash</span>
                        <input type="text" name="cash-render" class="cash-render form-control" placeholder="0.00 [pgup]">
                    </div>
                    <div class="credit-wrap input-group" style="display:none; margin-bottom: 10px;">
                        <span class="input-group-addon">Credit</span>
                        <input data-remote-list="{{ url('/api/creditnames') }}"
                               data-list-highlight="true"
                               data-list-value-completion="true"
                               class="name-search form-control credit-name" type="text" placeholder="Name"/>
                        <input type="text" name="partial-payment" class="partial-payment form-control" placeholder="Partial Payment">
                    </div>
                    <div class="change-wrap">
                        <span style="font-size:14pt;">Change : </span>
                        <span><strong class="total-change" style="font-size:28pt; color: red;">0.00</strong></span>
                    </div>
                    <div class="btn btn-primary btn-end">Press [end] when its done</div>
                </div>

                <div class="col-sm-9">
                    <span class="credit-trigger"></span>
                    <div class="table-responsive">
                        <table class="table table-stripped">
                            <thead><th>Qty.</th><th>Unit</th><th>Articles</th><th>U. Price</th><th>Discount</th><th>Amount</th><th>do</th></thead>
                            <tbody class="item-display"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div><span class="label label-primary">Powered</span> <span class="label label-primary">by</span> <span class="label label-primary">Team IO</span></div>
        </div>
    </div>

</div>
<div class="print-wrap" style="width:500px; display:none; margin:0 auto;">
    <div style="text-align:center;">
        <h3><em>BIGBYTE ENTERPRISES</em></h3>
        Poblacion, Siquijor, Siquijor &nbsp; Tel Nos. (035) 4809987; 4805513<br>
        Aliffod B. Espinosa - Prop. &nbsp; VAT Reg. TIN 182-525-761-000<br>
        Telefax (035) 4805515 &nbsp; Cell #. 09194112896</p>
        <strong><span class="payment-type">SALES</span> INVOICE</strong>
    </div>
    <div>
        <p>Charged to: ____________________&nbsp; Date: <u>{{ date('m-d-Y') }}</u><br>
            TIN: __________________________&nbsp; Terms: __________________<br>
            Address: ______________________&nbsp; OSCA/PWD ID No.:________<br>
            Business Style: _________________&nbsp; SC/PWD Sig: _____________</p>
    </div>
    <div>
        <table border="1" style="width:100%;">
            <thead><th>Qty.</th><th>Unit</th><th>ARTICLES</th><th>U. Price</th><th>Discount</th><th>Amount</th></thead>
            <tbody class="print-item-wrap">
            <tr><td colspan="5" style="text-align: right;">Total Sales (VAT Inclusive) &nbsp; </td><td class="total-amount print-total-amount"></td></tr>
            <tr><td colspan="5" style="text-align: right;">Less: VAT &nbsp; </td><td></td></tr>
            <tr>
                <td colspan="2">VATable Sales</td><td></td><td></td>
                <td style="text-align: right;">Amount: Net of VAT &nbsp;</td><td></td>
            </tr>
            <tr>
                <td colspan="2">VAT-Exempt Sales</td><td></td><td></td>
                <td style="text-align: right;">Less: SC/PWD Discount &nbsp;</td><td></td>
            </tr>
            <tr>
                <td colspan="2">Zero Rated Sales</td><td></td><td></td>
                <td style="text-align: right;">Amount Due &nbsp;</td><td></td>
            </tr>
            <tr>
                <td colspan="2">VAT Amount</td><td></td><td></td>
                <td style="text-align: right;">Add: VAT &nbsp;</td><td></td>
            </tr>
            <tr><td colspan="5" style="text-align: right;">TOTAL AMOUNT DUE &nbsp; </td><td class="total-amount"></td></tr>
            <tr><td colspan="5" style="text-align: right;"><span class="print-render">CASH RENDER</span> &nbsp; </td><td class="cash-render-clone"></td></tr>
            <tr class="hide-if-credit"><td colspan="5" style="text-align: right;">CHANGE &nbsp; </td><td class="total-change"></td></tr>
            </tbody>
        </table>
        <p>
            <div style="float:left;">
            ______________________ <br>
            &nbsp; &nbsp; &nbsp; &nbsp;
            Customer's Signature
            </div>

        <div style="float:left; text-align: center; margin-left:30px;">
            <u>{{$cashier}}</u><br>
            Cashier
        </div>

        <div style="float: right;">No. <span class="trans-code"></span></div>
        <div style="clear:both;"></div>
        </p>
        <p>
            30 Bklts[2x] 10501-12000 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            BIR Authority to Print No. <strong>2AU0001351417</strong><br>
            Date Issued: <strong><u>{{ date('m/d/Y') }} Valid until: {{ date('m/d/') }} {{ date('Y') + 5 }}</u></strong>
        </p>
    </div>
</div>
<!-- jQuery 2.1.4 -->
<script src="{{ url('assets/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<!-- optional for old browser support or to enhance look and feel -->
<script src="{{ url('remote-list/polyfiller.js') }}"></script>
<script type="text/javascript">
    (function () {
        var stateMatches = {
            'true': true,
            'false': false,
            'auto': 'auto'
        };
        var enhanceState = (location.search.match(/enhancelist\=([true|auto|false]+)/) || ['', 'auto'])[1];
        $(function () {
            $('.polyfill-type select')
                    .val(enhanceState)
                    .on('change', function () {
                        location.search = 'enhancelist=' + $(this).val();
                    });
        });

        webshims.setOptions('forms', {
            customDatalist: stateMatches[enhanceState]
        });
    })();

    webshims.polyfill('forms');
</script>
<script src="{{ url('assets/dist/js/remote-list.min.js') }}"></script>
<script src="{{ url('assets/dist/js/autoNumeric-min.js') }}"></script>
<script type="text/javascript">
    $(function () {

        var itemCode = '',
                credit = false,
                creditColor = 'black',
                price = '',
                transCode = '',
                date = new Date();

        transCode = String(date.getYear()) + String(parseInt(date.getMonth()) + 1) + String(date.getDate()) + String(date.getHours()) + String(date.getMinutes()) + String(date.getSeconds());

        //** Set total amount value
        function setAmountTotal()
        {
            var itemAmountSerialize = $('.item-amount').serializeArray();
            var itemAmount = 0;
            var itemTotalCount = 0;
            $.each(itemAmountSerialize, function(key,val){
                itemTotalCount++;
                itemAmount += parseFloat(val['value']);
            });
            if(itemAmount){ $('.total-amount').text(itemAmount.formatMoney(2,',','.')); }
            else{ $('.total-amount').text('0.00'); }
            if(itemTotalCount){ $('.total-item').text(itemTotalCount); }
            else{ $('.total-item').text(0); }
        }

        //** Set cash change
        function cashChange()
        {
            //** Set change
            var cashRender = parseFloat($('.cash-render').val().replace(',',''));
            var totalAmount = parseFloat($('.total-amount').text().replace(',',''));
            if(cashRender > totalAmount) {
                totalAmount = Math.floor(totalAmount * 100)/100;
                var totalChange = cashRender - totalAmount;
                $('.total-change').text(totalChange.formatMoney(2, ',', '.'));
            }
            else {
                $('.total-change').text('0.00');
            }
        }

        //Showing item in the list
        function showItem(itemInfo)
        {
            if(credit) {
                price = itemInfo['credit_price'];
                creditColor = 'red';
            }
            else {
                price = itemInfo['cash_price'];
                creditColor = 'black';
            }

            var itemDisplay = '<tr class="' + itemInfo['id'] + '" style="color:' + creditColor + '">';
            itemDisplay += '<input type="hidden" value="1" name="' + itemInfo['id'] + ',' + price + '" class="item-code">';
            itemDisplay += '<input type="hidden" name="item-left" value="' + itemInfo['quantity'] + '" class="item-left">';
            itemDisplay += '<input type="hidden" name="' + itemInfo['id'] + '" value="0" class="item-discount">';
            itemDisplay += '<td class="item-quantity" contenteditable="true" title="' + itemInfo['quantity'] + ' item/s left">1</td>';
            itemDisplay += '<td style="text-align:left;">' + itemInfo['unit'] + '/s</td>';
            itemDisplay += '<td>' + itemInfo['value'] + '</td>';
            itemDisplay += '<td class="item-price">' + price + '</td>';
            itemDisplay += '<td class="item-discount-input" contenteditable="true">0</td>';
            itemDisplay += '<td><input type="text" class="item-amount" style="border:none; width:50px;" name="item-amount" value="' + price + '"></td>';
            itemDisplay += '<td class="action-taken"><a href="javascript:;" class="item-cancel glyphicon glyphicon-remove-circle"></a></td>';
            itemDisplay += '</tr>';

            //** Add object code to existing json
            var itemKey = itemInfo['id'],
                    itemValue = itemInfo['value'],
                    itemFound = 0,
                    transLabel = 'Transaction : ';

            $('.trans-code').text(transCode);
            $('.trans-label').text(transLabel);

            if(itemCode.length) {
                var formatCode = '{' + itemCode + '}',
                        parseCode = JSON.parse(formatCode);

                itemCode += ', "' + itemKey + '" : "' + itemValue + '"';

                $.each(parseCode, function(codeKey, codeValue) {
                    if(itemKey==codeKey) { itemFound++; }
                });

                if (itemFound) {
                    //** Existing Item
                    var existItemQty = $('.' + itemKey).find('.item-quantity'),
                            existItemQtyVal = parseInt(existItemQty.text()),
                            addQty = existItemQtyVal + 1;
                    existItemQty.text(addQty);
                    $('.' + itemKey).find('.item-code').val(addQty);

                    var itemPrice = parseFloat($('.' + itemKey).find('.item-price').text());

                    var itemAmount = itemPrice * addQty;

                    if (itemAmount) {
                        $('.' + itemKey).find('.item-amount').val(itemAmount);
                    }
                    setAmountTotal();
                    cashChange();
                }
                else {
                    $('.item-display').prepend(itemDisplay);
                    setAmountTotal();
                    cashChange();
                }
            }
            else {
                itemCode += '"' + itemKey + '" : "' + itemValue + '"';
                $('.item-display').prepend(itemDisplay);
                setAmountTotal();
                cashChange();
            }

            $('.item-search').val('');

            var oldQuantity='';
            $('.item-quantity').on('click',function() {
                oldQuantity = parseInt($(this).text());
                $(this).text('');
            }).keyup(function() {
                var itemPrice = parseFloat($(this).parents('tr').find('.item-price').text()),
                        itemQuantity = parseInt($(this).text()),
                        itemLeft = parseInt($(this).parents('tr').find('.item-left').val()),
                        itemAmount = itemPrice * itemQuantity;
                if( !itemQuantity ||(itemAmount && itemQuantity <= itemLeft)) {
                    //$(this).parents('tr').find('.item-amount').val(itemAmount);
                    $(this).parents('tr').find('.item-amount').attr('value',itemAmount);
                    $(this).parents('tr').find('.item-code').val(itemQuantity);
                }
                else {
                    $(this).text(itemLeft);
                    alert('Woops, only ' + itemLeft + ' item/s left!');
                }
                setAmountTotal();
                cashChange();

            }).blur( function() {
                if(!$(this).text()) {
                    $(this).text(oldQuantity);
                }
            });

            var oldDiscount='';
            $('.item-discount-input').on('click',function() {
                oldDiscount = parseFloat($(this).text());
                $(this).text('');
            }).keyup(function() {
                var itemPrice = parseFloat($(this).parents('tr').find('.item-price').text()),
                        itemQty = parseFloat($(this).parents('tr').find('.item-quantity').text()),
                        itemDiscount = parseFloat($(this).text()),
                        itemAmount = itemQty * itemPrice,
                        newItemAmount = itemAmount - itemDiscount;
                if( itemDiscount ||(itemAmount > newItemAmount)) {
                    $(this).parents('tr').find('.item-amount').attr('value',newItemAmount);
                    $(this).parents('tr').find('.item-discount').val(itemDiscount);
                }
                if(!itemDiscount) {
                    $(this).parents('tr').find('.item-amount').attr('value',itemAmount);
                    $(this).val(0);
                }
                setAmountTotal();
                cashChange();

            }).blur( function() {
                if(!$(this).text()) {
                    $(this).text(oldDiscount);
                }
            });

            ///Reserve area

            //** Item cancel *********************************************************************
            $('.item-cancel').on('click', function() {

                var formatCodeAgain = '{' + itemCode + '}',
                        parseCodeAgain = JSON.parse(formatCodeAgain);

                if(parseCodeAgain) {
                    var keyToDelete = $(this).parents('tr').attr('class');

                    var itemFirstLoop = true
                    itemCode = '';
                    $.each(parseCodeAgain, function(key, val) {

                        if(key!=keyToDelete) {
                            if(itemFirstLoop) {
                                itemCode += '"' + key + '":"' + val + '"';
                            }
                            else {
                                itemCode += ', "' + key + '":"' + val + '"';
                            }
                            itemFirstLoop = false;
                        }
                    });
                }
                else {
                    itemCode = '';
                }

                $(this).parents('tr').remove();

                //** Item set total amount
                setAmountTotal();

                //** Set Change
                cashChange();
            });
            //**********************************************************************************

            //** Display change ****************************************************************
            $('.cash-render').keyup( function(){
                cashChange();
            });
            keyCode();
            cashChange();
        }

        function endTrans()
        {
            if($('.total-amount').text()!= '0.00') {
                var remarks = 0,
                        credit_info = '',
                        partial_payment = 0,
                        itemSelected = $('.item-code').serializeArray(),
                        itemDiscount = $('.item-discount').serializeArray(),
                        cashRender = $('.cash-render').val(),
                        totalItems = $('.total-item').text(),
                        totalAmount = parseFloat($('.total-amount').text()),
                        totalChange = $('.total-change').text(),
                        otherItems = $('.other-item').serializeArray();

                $('.cash-render-clone').text(cashRender);

                if (credit) {
                    remarks = 1;
                    credit_info = $('.credit-name').val();
                    partial_payment = parseFloat($('.partial-payment').val());
                    $('.cash-render-clone').text(partial_payment);
                }

                var totalVat = (totalAmount/1.12) *.12;

                $('.print-total-amount').text(totalVat.toFixed(2));

                if(cashRender!=''||credit_info!='') {
                    $('.print-item-wrap').prepend($('.item-display').html());
					$('.btn-end').text('Please wait...');
                    $.ajax({
                        type : 'POST',
                        url : '{{ url('cashier/sale') }}',
                        data : {
                            _token: '{{csrf_token()}}',
                            remarks: remarks,
                            transaction_number: transCode,
                            credit_info: credit_info,
                            cash_render: cashRender,
                            total_amount: Math.floor(totalAmount * 100)/100,
                            total_change: totalChange,
                            partial_payment: partial_payment,
                            discount: itemDiscount,
                            items: itemSelected,
                            other_items: otherItems
                        },
                        success : function(response) {

                            console.log(response);
                            var mywindow = window.open('', '','left=300, top=100, width=800, height=500, toolbar=0, scrollbars=0, status=0');

                            var htmlcontent = '<!DOCTYPE html>';
                            htmlcontent += '<html><head><style> .item-price,.item-discount-input{ text-align:center; } html,body { font-family:sans-serif; width:500px; margin:0 auto; font-size: 8pt; } td:nth-child(1),td:nth-child(2),td:nth-child(4),td:nth-child(5),td:nth-child(6) { text-align:center; } .action-taken{ display:none; }</style> </head><body>';
                            htmlcontent += $('.print-wrap').html();
                            htmlcontent += '</body></html>';

                            mywindow.document.write(htmlcontent);
                            mywindow.document.close();
                            mywindow.focus();
                            mywindow.print();
                            mywindow.close();
                            window.location.reload();
                        },
                        error : function() {
                            alert('Whoops, looks like something went wrong in saving sold item/s.');
                        }
                    });
                }
                else {
                    if (credit) {
                        alert('Whoops, dont forget to fill up name of customer');
                        $('.credit-name').focus()
                    }
                    else {
                        alert('Whoops, dont forget to put cash render.');
                        $('.cash-render').focus()
                    }

                }
            }
            else {
                alert('No item selected!');
            }
        }

        //** Hold keyboard event
        function keyCode()
        {
            var body = document.querySelector('body');
            body.onkeydown = function (e) {
                switch(e.keyCode){

                    //press enter
                    case 13:
                        if($('.cash-render:focus').length) {
                            $('.item-search').focus();
                            cashChange();
                        }

                        if($('.barcode:focus').length) {
                            $.ajax({
                                url: '{{ url() }}/api/product/' + $('.barcode').val(),
                                type: 'get',
                                success: function (response) {
                                    var product = JSON.parse(response);
                                    showItem(product[0]);
                                },
                                error: function () {
                                    alert('Whoops, looks like something went wrong in barcode search.');
                                }
                            });
                            $('.barcode').val('');
                        }

                        if($('.item-quantity:focus').length) { $('.item-search').focus(); }
                        break;
                    case 36:
                        $('.item-search').focus();
                        break;
                    case 33:
                        $('.cash-render').focus();
                        break;

                    case 35: //end
                        endTrans();
                        break;
                    case 19: //pause key //use credit
                        if($('.credit-trigger').text() != '') {
                            credit = false;
                            $('.credit-btn').css('background','none');
                            $('.credit-trigger').text('');
                            $('.credit-wrap').hide();
                            $('.cash-wrap').show();
                            $('.change-wrap').show();
                            $('.payment-type').text('SALES');
                            $('.print-render').text('CASH RENDER');
                            $('.hide-if-credit').show();
                        }
                        else {
                            credit = true;
                            $('.credit-btn').css('background','red');
                            $('.credit-trigger').html('<br><div class="label label-danger">Credit is active</div>');
                            $('.credit-wrap').show();
                            $('.cash-wrap').hide();
                            $('.change-wrap').hide();
                            $('.payment-type').text('CHARGE');
                            $('.print-render').text('PARTIAL PAYMENT');
                            $('.hide-if-credit').hide();
                        }
                        break;
                    case 27: //esc key //use barcode
                        $('.barcode').focus();
                        break;
                }
            };
        }

        //** Money formatter
        Number.prototype.formatMoney = function(decPlaces, thouSeparator, decSeparator) {
            var n = this,
                    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
                    decSeparator = decSeparator == undefined ? "." : decSeparator,
                    thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
                    sign = n < 0 ? "-" : "",
                    i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
                    j = (j = i.length) > 3 ? j % 3 : 0;
            return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
        };
        keyCode();
        $('input.item-search').remoteList({
            minLength: 0,
            maxLength: 0,
            select: function(){
                showItem($(this).remoteList('selectedData'));
            }
        });

        $('input.item-inquire').remoteList({
            minLength: 0,
            maxLength: 0,
            select: function(){
                console.log($(this).remoteList('selectedData'));
            }
        });

        $('.btn-end').on('click',function() {
            endTrans();
        });

        $('input.name-search').remoteList({
            minLength: 0,
            maxLength: 0,
            select: function(){
                console.log($(this).remoteList('selectedData'));
            }
        });

        //** AutoNumeric Plugins with defaults
        $('.cash-render').autoNumeric('init');

        /* START OF TOOLS ****************************************/
        $('.tool-select').on('change', function(){
            $('.tools').hide();
            switch($(this).val()) {
                case 'sa' :
                    $('.tools.sale-tool').show();
                    break;
                case 'aq' :
                    $('.tools.aquabyte-tool').show();
                    break;
                case 'el' :
                    $('.tools.eload-tool').show();
                    break;
                case 'ho' :
                    $('.tools.hands-on-tool').show();
                    break;
                case 'px' :
                    $('.tools.px-tool').show();
                    break;
                case 'se' :
                    $('.tools.se-tool').show();
                    break;
            }
        });

        /* START OF TOOLS ****************************************/
        $('.tool-amount').on('focus', function(){
            switch($('.tool-select').val()) {
                case 'aq' :
                    var price = $('.aqua-price').val()?parseFloat($('.aqua-price').val()):0,
                            quantity = $('.aqua-quantity').val()?parseFloat($('.aqua-quantity').val()):0;
                    $('.aqua-amount').val(price*quantity);
                    break;
                case 'el' :
                    var load = $('.el-load').val()?parseFloat($('.el-load').val()):0;
                    $('.el-amount').val(load);
                    break;
                case 'ho' :
                    var tin = $('.ho-tin').val(),
                            tout = $('.ho-tout').val(),
                            mins = 0,
                            hour = 0,
                            amount = parseFloat($('.ho-perhour').val()),
                            namount = 0,
                            perMins = amount/60;
                    $.ajax({
                        url: '{{url('convert-time')}}',
                        type: 'post',
                        data: { tin: tin, tout: tout, _token: '{{ csrf_token() }}' },
                        success: function(response) {
                            hour = parseInt(response/60);
                            mins = parseInt(response%60);
                            namount = (hour * amount) + (perMins * mins);
                            namount = namount.toFixed(2);
                            $('.ho-amount').val(namount);
                            $('.ho-time').val(hour +'.'+ mins);

                            //$('.ho-amount').val(namount);
                            //console.log(hour + ':' + mins + ' => ' + namount);
                        },
                        error: function() {
                            alert('Whoops, something went wrong!');
                        }
                    });
                    break;

                case 'px' :
                    var price = $('.px-price').val()?parseFloat($('.px-price').val()):0,
                            quantity = $('.px-quantity').val()?parseFloat($('.px-quantity').val()):0;
                    $('.px-amount').val(price*quantity);
                    break;

                case 'se' :
                    var price = $('.se-price').val()?parseFloat($('.se-price').val()):0,
                            quantity = $('.se-quantity').val()?parseFloat($('.se-quantity').val()):0;
                    $('.se-amount').val(price*quantity);
                    break;
            }
        });

        $('.add-btn').on('click', function(){
            $('.trans-code').text(transCode);
            $('.trans-label').text('Transaction : ');
            if(credit) { creditColor = 'red'; }
            else { creditColor = 'black'; }

            switch($('.tool-select').val()) {
                case 'aq' :
                    var discount = $('.aqua-discount').val()?parseFloat($('.aqua-discount').val()):0,
                            price = parseFloat($('.aqua-price').val()),
                            amount = parseFloat($('.aqua-amount').val()),
                            quantity = $('.aqua-quantity').val(),
                            amount = amount - discount,
                            amount = amount.toFixed(2),
                            otherDisplay = '<tr style="color:' + creditColor + '">';
                    otherDisplay += '<input type="hidden" name="aq" value="'+ quantity +','+ price +','+ discount +','+ amount +'" class="other-item">';
                    otherDisplay += '<td class="quantity">'+ quantity +'</td>';
                    otherDisplay += '<td>gal/s</td>';
                    otherDisplay += '<td>Aquabyte</td>';
                    otherDisplay += '<td>'+ price +'</td>';
                    otherDisplay += '<td class="discount-input">'+ discount +'</td>';
                    otherDisplay += '<td><input type="text" readonly class="item-amount" style="border:none; width:50px;" name="amount" value="' + amount + '"></td>';
                    otherDisplay += '<td class="action-taken"><a href="javascript:;" class="other-cancel glyphicon glyphicon-remove-circle"></a></td>';
                    otherDisplay += '</tr>';

                    $('.aqua-discount').val('');
                    $('.aqua-price').val('');
                    $('.aqua-amount').val('');
                    $('.aqua-quantity').val('');
                    break;

                case 'el' :
                    var discount = $('.el-discount').val()?parseFloat($('.el-discount').val()):0,
                            load = parseFloat($('.el-load').val()),
                            price = parseFloat($('.el-amount').val()),
                            network = $('.el-network').val(),
                            number = $('.el-number').val(),
                            amount = price - discount,
                            amount = amount.toFixed(2),
                            otherDisplay = '<tr style="color:' + creditColor + '">';
                    otherDisplay += '<input type="hidden" name="el" value="'+ load +','+ price +','+ discount +','+ amount +','+ network +','+ number +'" class="other-item">';
                    otherDisplay += '<td class="quantity">'+ load +'</td>';
                    otherDisplay += '<td>load</td>';
                    otherDisplay += '<td>E-Load - '+ network +'/'+ number +'</td>';
                    otherDisplay += '<td>'+ price +'</td>';
                    otherDisplay += '<td class="discount-input">'+ discount +'</td>';
                    otherDisplay += '<td><input type="text" readonly class="item-amount" style="border:none; width:50px;" name="amount" value="' + amount + '"></td>';
                    otherDisplay += '<td class="action-taken"><a href="javascript:;" class="other-cancel glyphicon glyphicon-remove-circle"></a></td>';
                    otherDisplay += '</tr>';

                    $('.el-network').val('');
                    $('.el-number').val('');
                    $('.el-load').val('');
                    $('.el-discount').val('');
                    $('.el-amount').val('');
                    break;

                case 'ho' :
                    var discount = $('.ho-discount').val()?parseFloat($('.ho-discount').val()):0,
                            time_used = $('.ho-time').val(),
                            perhour = parseFloat($('.ho-perhour').val()),
                            time_in = $('.ho-tin').val(),
                            time_out = $('.ho-tout').val(),
                            price = parseFloat($('.ho-amount').val()),
                            amount = price - discount,
                            amount = amount.toFixed(2),
                            otherDisplay = '<tr style="color:' + creditColor + '">';
                    otherDisplay += '<input type="hidden" name="ho" value="'+ perhour +','+ discount +','+ amount +','+ time_in +','+ time_out +','+ time_used +'" class="other-item">';
                    otherDisplay += '<td class="quantity">'+ time_used +'</td>';
                    otherDisplay += '<td>time</td>';
                    otherDisplay += '<td>Hands-on ('+ time_in +'-'+ time_out +')</td>';
                    otherDisplay += '<td>'+ perhour +'</td>';
                    otherDisplay += '<td class="discount-input">'+ discount +'</td>';
                    otherDisplay += '<td><input type="text" readonly class="item-amount" style="border:none; width:50px;" name="amount" value="' + amount + '"></td>';
                    otherDisplay += '<td class="action-taken"><a href="javascript:;" class="other-cancel glyphicon glyphicon-remove-circle"></a></td>';
                    otherDisplay += '</tr>';

                    $('.ho-time').val('');
                    $('.ho-tin').val('');
                    $('.ho-tout').val('');
                    $('.ho-discount').val('');
                    $('.ho-amount').val('');
                    break;

                case 'px' :
                    var discount = $('.px-discount').val()?parseFloat($('.px-discount').val()):0,
                            price = parseFloat($('.px-price').val()),
                            amount = parseFloat($('.px-amount').val()),
                            quantity = $('.px-quantity').val(),
                            amount = amount - discount,
                            amount = amount.toFixed(2),
                            otherDisplay = '<tr style="color:' + creditColor + '">';
                    otherDisplay += '<input type="hidden" name="px" value="'+ quantity +','+ price +','+ discount +','+ amount +'" class="other-item">';
                    otherDisplay += '<td class="quantity">'+ quantity +'</td>';
                    otherDisplay += '<td>pc/s</td>';
                    otherDisplay += '<td>Photocopy</td>';
                    otherDisplay += '<td>'+ price +'</td>';
                    otherDisplay += '<td class="discount-input">'+ discount +'</td>';
                    otherDisplay += '<td><input type="text" readonly class="item-amount" style="border:none; width:50px;" name="amount" value="' + amount + '"></td>';
                    otherDisplay += '<td class="action-taken"><a href="javascript:;" class="other-cancel glyphicon glyphicon-remove-circle"></a></td>';
                    otherDisplay += '</tr>';

                    $('.px-discount').val('');
                    $('.px-price').val('');
                    $('.px-amount').val('');
                    $('.px-quantity').val('');
                    break;

                case 'se' :
                    var discount = $('.se-discount').val()?parseFloat($('.se-discount').val()):0,
                            price = parseFloat($('.se-price').val()),
                            amount = parseFloat($('.se-amount').val()),
                            quantity = $('.se-quantity').val(),
                            desc = $('.se-desc').val(),
                            amount = amount - discount,
                            amount = amount.toFixed(2),
                            otherDisplay = '<tr style="color:' + creditColor + '">';
                    otherDisplay += '<input type="hidden" name="se" value="'+ quantity +','+ price +','+ discount +','+ amount +','+ desc +'" class="other-item">';
                    //otherDisplay += '<input type="hidden" name="se_desc" value="'+ desc +'" class="other-item">';
                    otherDisplay += '<td class="quantity">'+ quantity +'</td>';
                    otherDisplay += '<td></td>';
                    otherDisplay += '<td>'+ desc +'</td>';
                    otherDisplay += '<td>'+ price +'</td>';
                    otherDisplay += '<td class="discount-input">'+ discount +'</td>';
                    otherDisplay += '<td><input type="text" readonly class="item-amount" style="border:none; width:50px;" name="amount" value="' + amount + '"></td>';
                    otherDisplay += '<td class="action-taken"><a href="javascript:;" class="other-cancel glyphicon glyphicon-remove-circle"></a></td>';
                    otherDisplay += '</tr>';

                    $('.se-discount').val('');
                    $('.se-desc').val('');
                    $('.se-price').val('');
                    $('.se-amount').val('');
                    $('.se-quantity').val('');
                    break;

                default:
                    break;
            }

            $('.item-display').prepend(otherDisplay);
            setAmountTotal();
            cashChange();

            $('.other-cancel').on('click', function(){
                $(this).parents('tr').remove();
                setAmountTotal();
                cashChange();
            });

            $('.cash-render').keyup( function(){
                cashChange();
            });
        });
    });
</script>
</body>
</html>