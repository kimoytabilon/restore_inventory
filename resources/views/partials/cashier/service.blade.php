@extends('layouts.master')
@section('title','Timeline')

@section('content')
@include('partials.nav',['role'=>$role,'active'=>'tool'])
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Tool
            <small>Services</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?php
                    $t = $type;
                    $r = $when;
                    $er = $r!=''? explode('-',$r):'';
                    $er1 = isset($er[1])?$er[1]:'01';
                    $er2 = isset($er[2])?$er[2]:'';
                    $er3 = isset($er[0])?$er[0]:'';

                    $mos = [
                            '01'=>'January','02'=>'February','03'=>'March','04'=>'April',
                            '05'=>'May','06'=>'June','07'=>'July','08'=>'August',
                            '09'=>'September','10'=>'October','11'=>'November','12'=>'December',
                    ];

                    $tformat = ['Daily'=>$mos[$er1].' '.$er2,'Monthly'=>'the month of '.$mos[$er1],'Yearly'=>'year '.$er[0],'Overall'=>'overall'];
                    ?>
                    <div class="col-xs-12">
                        <select class="select-yr">
                            @for($yr=2016;$yr<=(2016+5);$yr++)
                                <option {{ $yr==$er3 ? 'selected':'' }}>{{ $yr }}</option>
                            @endfor
                        </select>

                        @if($t!=''&&($t=='Monthly' || $t=='Daily'))
                            <select class="select-mo">
                                @foreach($mos as $int=>$mo)
                                    <option {{ $int==$er1 ? 'selected':'' }} value="{{ $int }}">{{ $mo }}</option>
                                @endforeach
                            </select>
                        @endif

                        @if($t!=''&&$t=='Daily')
                            <select class="select-dy">
                                @for($dy=1;$dy<=31;$dy++)
                                    <?php $dy = $dy<10?'0'.$dy:$dy; ?>
                                    <option {{ $dy==$er2 ? 'selected':'' }}>{{ $dy }}</option>
                                @endfor
                            </select>
                        @endif
                        <a href="{{url($role.'/service/'.$t.'/'.$r)}}" class="btn btn-primary btn-xs btn-flat show-btn">show</a>
                        @if($t!=''&&($t=='Yearly' || $t=='Monthly' || $t=='Daily'))
                            <a href="{{ url($role.'/service/Daily/'.date('Y-m-d')) }}" class="btn btn-info btn-xs btn-flat">today</a>
                            <a href="{{ url($role.'/service/Monthly/'.date('Y-m')) }}" class="btn btn-warning btn-xs btn-flat">this month</a>
                            <a href="{{ url($role.'/service/Yearly/'.date('Y')) }}" class="btn btn-danger btn-xs btn-flat">this year</a>
                            <a href="javascript:;" class="btn btn-default btn-xs btn-flat print-btn">print</a>
                        @endif
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <th>Time</th>
                        <th>Description</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th>Collection</th>
                        <th>Sub Total</th>
                        {!! $role!='cashier'?'<th></th>':'' !!}
                        </thead>
                        <tbody class="show-result">
                        <?php $overall_amount = $pxs['overall_amount']; ?>
                        <?php $overall_collection = $pxs['overall_collection']; ?>
                        {{ array_forget($pxs, ['overall_amount', 'overall_collection']) }}
                        @foreach($pxs as $pxKey => $pxVal)
                            @if($pxKey!='')
                                <tr class="bg-info">
                                    <input type="hidden" class="credit-name-slug" name="credit-name-slug" value="{{str_slug($pxKey)}}">
                                    <td><span class="credit-name">{{$pxKey}}</span> (Credit)</td>
                                    <td></td>
                                    <td>{{$pxVal['total_amount']}}</td>
                                    <td>{!!$pxVal['total_amount']?'Payable':'Paid'!!}</td>
                                    <td class="collection-total">{{$pxVal['total_collection']}}</td>
                                    <td class="credit-total" contenteditable="true">{{$pxVal['total_amount'] - $pxVal['total_collection']}}</td>
                                </tr>
                            @endif
                            {{ array_forget($pxVal, ['total_amount', 'total_collection']) }}
                            <tbody class="{{str_slug($pxKey)}}">
                            @foreach($pxVal as $px)
                                <tr>
                                    <input type="hidden" name="id" class="credit-id" value="{{$px['id']}}">
                                    <td><a href="{{url($role.'/findtrans?q='.$px['transaction'])}}">{{$px['transaction']}}</a> - {{ $px['created_at']->format('h:i a') }}</td>
                                    <td>{{ $px['desc'] }}</td>
                                    <td>{{ $px['amount'] }}</td>
                                    <td>{!! $px['remarks']==1?'Credit':'Paid' !!}</td>
                                    <td class="collection">{{ $px['collection'] }}</td>
                                    <td class="credit-subtotal">{{ $px['amount'] - $px['collection'] }}</td>
                                    {!! $role!='cashier'?'<td><a href="javascript:;" class="glyphicon glyphicon-remove-circle remove-btn"></a></td>':'' !!}
                                </tr>
                            @endforeach
                            </tbody>
                        @endforeach
                        <tr class="bg-primary">
                            <td class="text-center">Total Cash</td>
                            <td></td>
                            <td class="total-amount">{{$overall_amount}}</td>
                            <td></td>
                            <td class="total-collection">{{$overall_collection}}</td>
                            <td class="total-overall">{{$overall_amount + $overall_collection}}</td><td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

@include('partials.footer')

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready( function (){
            var v = '{{$r}}';

            $('.select-yr').on('change', function() {
                if($('.select-mo').length&&!$('.select-dy').length) { v = $('.select-yr').val() + '-' + $('.select-mo').val(); $('.show-btn').attr('href', '{{url($role.'/service/'.$t)}}/'+v); }
                if($('.select-mo').length&&$('.select-dy').length) { v = $('.select-yr').val() + '-' + $('.select-mo').val() + '-' + $('.select-dy').val(); $('.show-btn').attr('href', '{{url($role.'/service/'.$t)}}/'+v); }
                if(!$('.select-mo').length&&!$('.select-dy').length) { v = $('.select-yr').val(); $('.show-btn').attr('href', '{{url($role.'/service/'.$t)}}/'+v); }
            });

            $('.select-mo').on('change', function() {
                if($('.select-mo').length&&!$('.select-dy').length) { v = $('.select-yr').val() + '-' + $('.select-mo').val(); $('.show-btn').attr('href', '{{url($role.'/service/'.$t)}}/'+v); }
                if($('.select-mo').length&&$('.select-dy').length) { v = $('.select-yr').val() + '-' + $('.select-mo').val() + '-' + $('.select-dy').val(); $('.show-btn').attr('href', '{{url($role.'/service/'.$t)}}/'+v); }
                if(!$('.select-mo').length&&!$('.select-dy').length) { v = $('.select-yr').val(); $('.show-btn').attr('href', '{{url($role.'/service/'.$t)}}/'+v); }
            });

            $('.select-dy').on('change', function() {
                if($('.select-mo').length&&!$('.select-dy').length) { v = $('.select-yr').val() + '-' + $('.select-mo').val(); $('.show-btn').attr('href', '{{url($role.'/service/'.$t)}}/'+v); }
                if($('.select-mo').length&&$('.select-dy').length) { v = $('.select-yr').val() + '-' + $('.select-mo').val() + '-' + $('.select-dy').val(); $('.show-btn').attr('href', '{{url($role.'/service/'.$t)}}/'+v); }
                if(!$('.select-mo').length&&!$('.select-dy').length) { v = $('.select-yr').val(); $('.show-btn').attr('href', '{{url($role.'/service/'.$t)}}/'+v); }
            });

            $('.print-btn').on('click', function() {

                var mywindow = window.open('', '','left=300, top=100, width=800, height=500, toolbar=0, scrollbars=0, status=0');

                var htmlcontent = '<!DOCTYPE html>';
                htmlcontent += '<html><head> <style> body,table{ border:1px solid #333; padding:5px; max-width:700px; width:100%; margin:0 auto; font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; } .breadcrumb, form{ display:none; } td{ text-align:center; }</style> </head><body>';
                htmlcontent += $('.content-wrapper').html();
                htmlcontent += '</body></html>';

                mywindow.document.write(htmlcontent);
                mywindow.document.close();
                mywindow.focus();
                mywindow.print();
                mywindow.close();
            });

            $('.remove-btn').on('click', function() {
                var id = $(this).parents('tr').find('.credit-id').val(),
                        collection = $(this).parents('tr').find('.collection').text(),
                        remarks = $(this).parents('tr').find('.remarks').text(),
                        subTotal = $(this).parents('tr').find('.credit-subtotal').text(),
                        amount = 0;
                if(remarks == 'Credit' && collection==0){ amount = 0; }
                if(remarks == 'Credit' && collection!=0){ amount = collection; }
                if(remarks == 'Paid'){ amount = subTotal; }

                if(confirm('You are going to remove this list. Continue?')) {
                    $.ajax({
                        url: '{{url($role.'/se-destroy')}}',
                        type: 'post',
                        data: {_token: '{{ csrf_token() }}', id: id, amount: amount, date: '{{$r}}'},
                        success: function (response) {
                            if(response==0) {
                                alert('Whoops, cashier already installed an amount. Request failed.');
                                window.location.reload();
                            }
                        },
                        error: function () {
                            alert('Whoops, pay credit request looks like something went wrong.');
                        }
                    });
                    $(this).parents('tr').empty();
                }
            });


        });
    </script>
@endsection