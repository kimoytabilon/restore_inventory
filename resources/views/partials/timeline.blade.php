@extends('layouts.master')
@section('title','Timeline')

@section('content')
@include('partials.nav',['role'=>$role,'active'=>'timeline'])
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>{{isset($_GET['name'])?$_GET['name']:''}} Timeline</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <ul class="timeline">
            @foreach($logs as $userlog)
                <li class="time-label">
                    <span class="bg-red">
                        {{ $userlog[0]['created_at']->format('j F Y') }}
                        <!--10 Feb. 2014-->
                    </span>
                </li>
                @foreach($userlog as $log)
                    <!-- timeline item  [blue, aqua, yellow, purple, maroon, ]-->
                    <li>
                        <!-- timeline icon -->
                        <i class="fa fa-{{$style[$log['type']]['icon']}} bg-{{$style[$log['type']]['color']}}"></i>
                        <div class="timeline-item">
                            <span class="time"><i class="fa fa-clock-o"></i> {{$log['created_at']->format('h:i a')}}</span>

                            <h3 class="timeline-header"><a href="javascript:;">{{$log['title']}}</a></h3>

                            <!--div class="timeline-body">
                                ...
                                Content goes here
                            </div>

                            <div class="timeline-footer">
                                <a class="btn btn-primary btn-xs">...</a>
                            </div-->
                        </div>
                    </li>
                    <!-- END timeline item -->
                @endforeach
            @endforeach
        </ul>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

@include('partials.footer')

@endsection