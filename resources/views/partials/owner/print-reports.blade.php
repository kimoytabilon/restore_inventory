<!DOCTYPE html>
<html>
<head>
    <title>Print</title>
    <style type="text/css">
        html,body {
            font-family:sans-serif;
            width:900px;
            margin:0 auto;
            font-size: 9pt;
        }
        .text-right {
            text-align: right;
        }
        .text-center {
            text-align: center;
        }
        .bold {
            font-weight: bold;
        }
    </style>
</head>

<body onload="">
<?php
$r = isset($_GET['r'])?$_GET['r']:'';
$er = $r!=''? explode('-',$r):'';

$mos = [
        '01'=>'January',
        '02'=>'February',
        '03'=>'March',
        '04'=>'April',
        '05'=>'May',
        '06'=>'June',
        '07'=>'July',
        '08'=>'August',
        '09'=>'September',
        '10'=>'October',
        '11'=>'November',
        '12'=>'December',
];
?>
<div style="text-align:center;">
    <h1><em>BIGBYTE ENTERPRISES</em></h1>
    <?php $count = count(explode('-',$r)); ?>
    <h2><?php switch($count){
            case 3: echo 'DAILY'; break;
            case 2: echo 'MONTHLY'; break;
            case 1: echo 'YEARLY'; break;
        }; ?> REPORT</h2>
</div>

<div>
    <p class="text-right bold">Date : <?php switch($count){
            case 3: echo $mos[$er[1]].' '.$er[2].', '.$er[0]; break;
            case 2: echo $mos[$er[1]].', '.$er[0]; break;
            case 1: echo $er[0]; break;
        }; ?></p>
    <table border="1" style="width:100%;">

        <thead>
        <tr><th colspan="10">SALES SERVICES</th></tr>
        <tr>
        <th>#.</th>
        <th>Description</th>
        <th>Item Left</th>

        <th>Item/s Sold (Cash)</th>
        <th>Cash Price</th>
        <th>Cash Total</th>

        <th>Item/s Sold (Credit)</th>
        <th>Credit Price</th>
        <th>Credit Total</th>

        <th>Collection</th>
        </tr>
        </thead>
        <tbody>

        <?php $x=1; ?>
        @foreach($items as $key=>$item)
            <tr>
                <td class="text-center">{{$x++}}</td>
                <td>{{ $item['description'] }}</td>
                <td class="text-center">{{ $item['quantity'] }}</td>

                <td class="text-center">{{ $item['items_sold_w_cash'] }}</td>
                <td class="text-center">{{ $item['item_cash'] }}</td>
                <td class="text-center">{{ $item['sold_cash'] }}{{ count(explode('.',$item['sold_cash']))==1?'.00':'' }}</td>

                <td class="text-center">{{ $item['items_sold_w_credit'] }}</td>
                <td class="text-center">{{ $item['item_credit'] }}</td>
                <td class="text-center">{{ $item['sold_credit'] }}{{ count(explode('.',$item['sold_credit']))==1?'.00':'' }}</td>
            </tr>
        @endforeach

        </tbody>
    </table>

</div>
<script type="text/javascript">
    //window.print();
    var body = document.querySelector('body');
     body.onkeydown = function (e) {
         switch(e.keyCode) {
         //esc key
             case 27:
             window.close();
             break;
         }
     };
</script>
</body>
</html>