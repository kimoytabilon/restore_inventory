@extends('layouts.master')
@section('title','Manage Product')

@section('content')
@include('partials.nav',['role'=>$profile->role,'active'=>'manage-product'])
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Manage Products
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Products</a></li>
            <li class="active">List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row add-product-form">
            <form action="{{ url('/'.$profile->role.'/manage-product/') }}" method="post">
                {!! csrf_field() !!}
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><small>Add Product</small> <small class="label label-danger">{{ session('error')?session('error'):'' }}</small></h3>
                        <div class="box-tools">
                            <button type="submit" class="btn btn-success btn-sm btn-block btn-flat product-save">Save <span class="glyphicon glyphicon-ok"></span></button>
                        </div>
                        <div class="box-tools" style="right:90px;">
                            <button type="reset" class="btn btn-warning btn-sm btn-block btn-flat add-product-close">Close <span class="glyphicon glyphicon-remove"></span></button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th style="width:20%;">Category</th>
                                <th style="width:30%;">Description</th>
                                <th>Qty</th>
                                <th>Unit</th>
                                <th>Orig</th>
                                <th>Cash</th>
                                <th>Credit</th>
                                <th>Barcode</th>
                            </tr>
                            <tr>
                                <td><input list="nameslist" type="text" class="form-control product-value  product-value-name" name="name" value="{{old('name')}}"></td>
                                <td><input type="text" class="form-control product-value" name="description" list="productDescription" style="width:100%;" value="{{old('description')}}"></td>
                                <td><input type="text" class="form-control product-value" placeholder="0" name="quantity" style="width:100%;" value="{{old('quantity')}}"></td>
                                <td><input type="text" class="form-control product-value" placeholder="pc/box" name="unit" style="width:100%;" value="{{old('unit')}}"></td>
                                <td><input type="text" class="form-control product-value" placeholder="00.00" name="original_price" style="width:100%;" value="{{old('original_price')}}"></td>
                                <td><input type="text" class="form-control product-value" placeholder="00.00" name="cash_price" style="width:100%;" value="{{old('cash_price')}}"></td>
                                <td><input type="text" class="form-control product-value" placeholder="00.00" name="credit_price" style="width:100%;" value="{{old('credit_price')}}"></td>
                                <td><input type="text" class="form-control product-value" placeholder="#" name="code" style="width:100%"></td>
                                <datalist id="nameslist">
                                    @foreach($names as $name)
                                        <option value="{{ $name }}">
                                    @endforeach
                                </datalist>
                            </tr>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
            </form>
        </div>

        @foreach($products as $keys=>$vals)
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <input type="hidden" class="product-name" name="product-name" value="{{ $keys }}">
                            <h3 class="box-title">{{ $keys }}</h3>
                            <!--div class="box-tools">
                                <div class="input-group" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                                    <div class="input-group-btn">
                                        <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div-->
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr>
                                    <th>#</th>
                                    <th>Barcode</th>
                                    <th style="width:30%;">Description</th>
                                    <th>Qty</th>
                                    <th>Unit</th>
                                    <th>Orig</th>
                                    <th>Cash</th>
                                    <th>Credit</th>
                                    <th></th>
                                </tr> <?php $x=1; ?>
                                @foreach($vals as $product)
                                <tr {!! $product['quantity']==0?' style="color:red"':'' !!}>
                                    <td>{{ $x++ }}</td>
                                    <input type="hidden" class="product-id" value="{{ $product['id'] }}">
                                    <td><input class="product-info no-style-textbox product-code" type="text" name="code" value="{{ $product['code'] }}" style="width:100%;"></td>
                                    <td><input class="product-info no-style-textbox product-description" type="text" name="description" value="{{ $product['description'] }}" style="width:100%;"></td>
                                    <td><input class="product-info no-style-textbox product-quantity" type="text" name="quantity" value="{{ $product['quantity'] }}" style="width:100%;"></td>
                                    <td><input class="product-info no-style-textbox product-unit" type="text" name="unit" value="{{ $product['unit'] }}" style="width:100%;"></td>
                                    <td><input class="product-info no-style-textbox product-original" type="text" name="original_price" value="{{ $product['original_price'] }}" style="width:100%;"></td>
                                    <td><input class="product-info no-style-textbox product-cash" type="text" name="cash_price" value="{{ $product['cash_price'] }}" style="width:100%;"></td>
                                    <td><input class="product-info no-style-textbox product-credit" type="text" name="credit_price" value="{{ $product['credit_price'] }}" style="width:100%;"></td>
                                    <td>
                                        <a href="javascript:;" class="glyphicon glyphicon-pencil product-patch-btn"></a>
                                    </td>
                                    <td>
                                        <a href="javascript:;" class="glyphicon glyphicon-remove product-destroy-btn"></a>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
            </div>
        @endforeach
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<datalist id="productName"></datalist>
<datalist id="productDescription"></datalist>

@include('partials.footer')

<!-- Control Sidebar -->
@include('partials.control')
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready( function (){
        $('.add-product-btn').on('click', function (){
            $('.add-product-form').show(150);
        });
        $('.add-product-close').on('click', function (){
            $('.add-product-form').hide(150);
        });

        var appendProductName;
        var appendProductDescription;

        $.each($('.product-name').serializeArray(), function (key,productName){
            appendProductName += '<option value="' + productName['value'] + '">';
        });
        $.each($('.product-description').serializeArray(), function (key,productDescription){
            appendProductDescription += '<option value="' + productDescription['value'] + '">';
        });

        //$('#productName').html(appendProductName);
        //$('#productDescription').html(appendProductDescription);

        ///owner/manage-product/
        //$('.product-save').on('click', function (){
           //console.log($('.product-value').serializeArray());
        //});

        $('.product-patch-btn').on('click', function (){
            var product = $(this).parents('tr');

            $.ajax({
                url : '{{ url("/".$profile->role."/manage-product") }}/' + product.find('.product-id').val(),
                type : 'POST',
                data : {
                    '_token':'{{ csrf_token() }}',
                    '_method':'PATCH',
                    'code':product.find('.product-code').val(),
                    'name':$(this).parents('.row').find('.product-name').val(),
                    'description':product.find('.product-description').val(),
                    'quantity':product.find('.product-quantity').val(),
                    'unit':product.find('.product-unit').val(),
                    'original_price':product.find('.product-original').val(),
                    'cash_price':product.find('.product-cash').val(),
                    'credit_price':product.find('.product-credit').val(),
                },
                success : function (response){
                    product.find('.product-info').css('color','green');
                },
                error : function (){
                    product.find('.product-info').css('color','red');
                    alert('Whoops, looks like something went wrong. Duplicate code found!');
                }
            });
        });

        $('.product-destroy-btn').on('click', function (){
            var product = $(this).parents('tr');

            if(confirm('Remove this item?'))
            $.ajax({
                url : '{{ url("/".$profile->role."/manage-product") }}/' + product.find('.product-id').val(),
                type : 'POST',
                data : {
                    '_token':'{{ csrf_token() }}',
                    '_method':'DELETE'
                },
                success : function (response){
                    product.remove();
                },
                error : function (){
                    product.find('.product-info').css('color','red');
                    alert('Whoops, looks like something went wrong.');
                }
            });
        });

    });
</script>
@endsection