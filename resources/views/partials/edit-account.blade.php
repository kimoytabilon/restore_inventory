@extends('layouts.master')
@section('title','My Acount')

@section('content')

@include('partials.nav',['role'=>$role, 'active'=>'edit-account'])

<style>
    .single-dropzone {
        .dz-image-preview, .dz-file-preview {
            display: none;
        }
    }
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            My Account
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Edit <small class="label label-success"></small><small class="label label-danger"></small></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <form enctype="multipart/form-data" id="imageUploadForm" method="post">
                    <table class="table table-bordered">
                        <thead>
                        <th>Image</th>
                        <th>Email</th>
                        <th>Old Password</th>
                        <th>New Password</th>
                        <th>Re Enter Password</th>
                        <th></th>
                        </thead>
                        <tbody>
                        <tr>
                            {{csrf_field()}}
                            <td class="img-td"><input type="file" name="img" class="no-style-textbox account-img" style="width:60px;"></td>
                            <td><input class="no-style-textbox account-email" type="text" name="email" value="{{$info->email}}" style="width:220px;"></td>
                            <td><input class="no-style-textbox old-password" type="password" name="old_password" placeholder="Old Password" style="width:160px;"></td>
                            <td><input class="no-style-textbox new-password" type="password" name="new_password" placeholder="New Password" style="width:160px;"></td>
                            <td><input class="no-style-textbox re-new-password" type="password" name="re_password" placeholder="Re Enter Password" style="width:160px;"></td>
                            <td>
                                <button class="glyphicon glyphicon-save btn btn-flat btn-xs btn-primary save-account-btnx"></button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    </form>
                </div>
            </div>

        </div>

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

@include('partials.footer')

@endsection

@section('script')
<script type="text/javascript">
    $(document).ready( function (){
        $('#imageUploadForm').on('submit',(function(e) {
            e.preventDefault();
            var formData = new FormData(this);
            var newPsw = $('.new-password').val(),
                    oldPsw = $('.old-password').val(),
                    img = $('.account-img').val(),
                    renewPsw = $('.re-new-password').val();
            if(newPsw!=''&&newPsw == renewPsw) {
                $.ajax({
                    type:'POST',
                    url: '{{url($role.'/update-account')}}',
                    data:formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success:function(response){
                        if(response==1) {
                            $('.label-danger').text('');
                            $('.label-success').text('Password successfully saved!');
                            $('.old-password').val('');
                            $('.new-password').val('');
                            $('.re-new-password').val('');
                        }
                        else {
                            $('.label-success').text('');
                            $('.label-danger').text('Whoops, old password incorrect!');
                        }
                    },
                    error: function(){
                        alert('Whoops, something went wrong!');
                    }
                });
            }
            else {
                $('.label-success').text('');
                $('.label-danger').text('Whoops, password did not match! Please try again.');
                $('.re-new-password').focus();
            }
        }));
    });
</script>
@endsection