<ul class="dropdown-menu">
    <!-- User image -->
    <li class="user-header">
        <img src="{{ url('assets/dist/img/'.$profile->img) }}" class="img-circle" alt="User Image">
        <p>
            {{ $profile->name }}
            <small>{{ ucfirst($profile->role) }}<br />Member since {{ \Carbon\Carbon::parse($profile->created_at)->diffForHumans() }}</small>
        </p>
    </li>
    <!-- Menu Footer-->
    <li class="user-footer">
        <div class="pull-left">
            <a href="{{url('/lockscreen')}}" class="btn btn-default btn-flat">Lockscreen</a>
        </div>
        <div class="pull-right">
            <a href="{{ url('/logout') }}" class="btn btn-default btn-flat">Logout</a>
        </div>
    </li>
</ul>