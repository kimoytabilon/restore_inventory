@extends('layouts.master')
@section('title','Monthly Sales Report')

@section('content')
@include('partials.nav',['role'=>$role,'active'=>'sale'])
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <?php
    $t = isset($_GET['t'])?$_GET['t']:'';
    $r = isset($_GET['r'])?$_GET['r']:'2016';
    $er = $r!=''? explode('-',$r):'';
    $er1 = isset($er[1])?$er[1]:'01';
    $er2 = isset($er[2])?$er[2]:'';
    $aq_no = 1;
    $sa_no = 1;
    $se_no = 1;
    $el_no = 1;
    $ho_no = 1;
    $px_no = 1;
    $su_no = 1;
    $mos = [
            '01'=>'January','02'=>'February','03'=>'March','04'=>'April',
            '05'=>'May','06'=>'June','07'=>'July','08'=>'August',
            '09'=>'September','10'=>'October','11'=>'November','12'=>'December',
    ];

    $tformat = ['Daily'=>$mos[$er1].' '.$er2,'Monthly'=>'the month of '.$mos[$er1],'Yearly'=>'year '.$er[0],'Overall'=>'overall'];
    $lists = $products;
    ?>

            <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{{$t}} Report <small>{{$t!='Overall'?$r:''}}</small></h1>
        @if($t!=''&&($t=='Yearly' || $t=='Monthly' || $t=='Daily'))
            <form method="get" action="{{ url($role.'/manage-sale') }}">
                <br>
                <input type="hidden" name="n" value="sale">
                <input type="hidden" name="t" value="{{ $t }}">
                <input type="hidden" name="r" class="r" value="{{ date('Y-m-d') }}">

                <select class="select-yr">
                    @for($yr=0;$yr<=5;$yr++)
                        <option>{{ $yr + 2016 }}</option>
                    @endfor
                </select>

                @if($t!=''&&($t=='Monthly' || $t=='Daily'))
                    <select class="select-mo">
                        @foreach($mos as $int=>$mo)
                            <option {{ $int==$er1 ? 'selected':'' }} value="{{ $int }}">{{ $mo }}</option>
                        @endforeach
                    </select>
                @endif

                @if($t!=''&&$t=='Daily')
                    <select class="select-dy">
                        @for($dy=1;$dy<=31;$dy++)
                            <?php $dy = $dy<10?'0'.$dy:$dy; ?>
                            <option {{ $dy==$er2 ? 'selected':'' }}>{{ $dy }}</option>
                        @endfor
                    </select>
                @endif
                <input type="submit" class="btn btn-primary btn-xs btn-flat" value="show">
                <a href="javascript:;" class="btn btn-primary btn-xs print-btn btn-flat">print</a>
            </form>
        @endif
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Sales Report</a></li>
            <li class="active">{{ $t }}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{$t}} Recap Report</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="chart-legend clearfix list-inline">
                                    <li><i class="fa fa-circle-o text-red"></i> Sale</li>
                                    @foreach($services as $service)
                                        <li><i class="fa fa-circle-o text-{{$service['color']}}"></i> {{$service['desc']}}</li>
                                    @endforeach
                                </ul>
                            </div><!-- /.col -->
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <!--p class="text-center">
                                    <strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong>
                                </p-->
                                <div class="chart">
                                    <!-- Sales Chart Canvas -->
                                    <canvas id="areaChart" style="height: 180px; width: 703px;" width="703" height="180"></canvas>
                                </div><!-- /.chart-responsive -->
                            </div><!-- /.col -->
                            <div class="col-md-4">
                                <!--p class="text-center">
                                    <strong>Goal Completion</strong>
                                </p-->
                                <div class="progress-group">
                                    <span class="progress-text">Available Products</span>
                                    <span class="progress-number"><b>{{$recap['avail_products']}}</b>/{{$recap['all_products']}}</span>
                                    <div class="progress sm">
                                        <div class="progress-bar progress-bar-aqua" style="width: {{($recap['avail_products']/$recap['all_products'])*100}}%"></div>
                                    </div>
                                </div><!-- /.progress-group -->
                                <div class="progress-group number-of-transaction"></div><!-- /.progress-group -->
                                <div class="progress-group">
                                    <span class="progress-text">Items Sold</span>
                                    <span class="progress-number"><b>{{$recap['item_sold']}}</b>/{{$recap['item_left']}}</span>
                                    <div class="progress sm">
                                        <div class="progress-bar progress-bar-green" style="width: {{($recap['item_sold']/$recap['item_left'])*100}}%"></div>
                                    </div>
                                </div><!-- /.progress-group -->
                                <div class="progress-group number-of-credit-transaction"></div><!-- /.progress-group -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- ./box-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block border-right">
                                    <h5 class="description-header total-cash-amount">0.00</h5>
                                    <span class="description-text">TOTAL CASH</span>
                                </div><!-- /.description-block -->
                            </div><!-- /.col -->
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block border-right">
                                    <h5 class="description-header total-discount-amount">0.00</h5>
                                    <span class="description-text">TOTAL DISCOUNT</span>
                                </div><!-- /.description-block -->
                            </div><!-- /.col -->
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block border-right">
                                    <h5 class="description-header total-credit-amount">0.00</h5>
                                    <span class="description-text">TOTAL CREDIT</span>
                                </div><!-- /.description-block -->
                            </div><!-- /.col -->
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block">
                                    <h5 class="description-header total-collection-amount">0.00</h5>
                                    <span class="description-text">TOTAL COLLECTION</span>
                                </div><!-- /.description-block -->
                            </div>
                        </div><!-- /.row -->
                    </div><!-- /.box-footer -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div>




        <div class="row">
            <div class="pull-right col-sm-5{{--$role!='cashier'?'':'col-sm-12'--}} col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Summary <small>for {{$tformat[$t]}}</small></h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="chart-responsive">
                                    <canvas id="pieChart" height="160" width="205" style="width: 205px; height: 160px;"></canvas>
                                </div><!-- ./chart-responsive -->
                            </div><!-- /.col -->
                            <div class="col-md-4">
                                <ul class="chart-legend clearfix">
                                    <li><i class="fa fa-circle-o text-red"></i> Sale</li>
                                    @foreach($services as $service)
                                        <li><i class="fa fa-circle-o text-{{$service['color']}}"></i> {{$service['desc']}}</li>
                                    @endforeach
                                </ul>
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.box-body -->
                    <div class="box-footer no-padding"><br>
                        <div class="col-xs-12">
                        <table class="table table-bordered">
                            <thead>
                            <th>Type</th>
                            <th>Payment</th>
                            <th>Cash</th>
                            <th>Credit</th>
                            </thead>
                            <tbody>
                            <tr class="bg-red">
                                <td><a href="javascript:;" style="color:#fff;">Sales</a> <span class="pull-right">P</span></td>
                                <td>{{ ($products['overalltotal_cash'] - $products['overalltotal_cash_discount']) + $products['overalltotal_collection'] }}</td>
                                <td>{{ $products['overalltotal_today'] }}</td>
                                <td>{{ $products['overalltotal_credit'] - ($products['overalltotal_credit_discount'] + $products['overalltotal_collection']) }}</td>
                            </tr>

                            @foreach($services as $service)
                                <tr class="bg-{{$service['color']}}">
                                    <td><a href="{{$service['url']}}" style="color:#fff;">{{$service['desc']}}</a> <span class="pull-right">P</span></td>
                                    <td>{{$service['amount'] + $service['collection']}}</td>
                                    <td>{{$service['cash']}}</td>
                                    <td>{{$service['credit'] - $service['collection']}}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        </div>
                    </div><!-- /.footer -->
                </div>
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Unpaid Credit <small>for {{$tformat[$t]}}</small></h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive"">
                    <table class="table table-bordered">
                        <thead>
                        <th>Name</th>
                        <th>Amount</th>
                        <th>Collection</th>
                        <th>Total</th>
                        <th>Date</th>
                        </thead>
                        <tbody>
                        @foreach($credits as $keys=>$creditvals)
                            <tr class="{{str_slug($keys)}}" style="background:#00c0ef">
                                <td>{{$keys}}</td>
                                <td>{{ $creditvals['total_amount']}}</td>
                                <td>{{ $creditvals['total_partial'] }}</td>
                                <input type="hidden" class="actual-amount" name="actual-amount" value="{{$creditvals['total_amount'] - $creditvals['total_partial'] }}">
                                <td class="balance-amount" contenteditable="true">{{$creditvals['total_amount'] - $creditvals['total_partial'] }}</td>
                                <td>
                                    <button class="btn btn-warning btn-xs pay-balance">Pay Balance</button>
                                    <span class="credit-slug" style="display: none;">{{str_slug($keys)}}</span>
                                    <span class="credit-name" style="display: none;">{{$keys}}</span>
                                </td>

                            </tr>
                            {{ array_forget($creditvals,['total_discount','total_amount','total_partial']) }}

                            @foreach($creditvals as $key=>$credit)
                                <tr class="{{str_slug($keys)}}">
                                    <td><a href="{{url($role.'/findtrans?q='.$key)}}">{{$key}}</td>
                                    <td>{{$credit['amount']}}</td>
                                    <td>{{$credit['partial']}}</td>
                                    <td>{{$credit['amount'] - $credit['partial']}}</td>
                                    <td>{{$credit['created_at']}}</td>
                                </tr>
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Sales by cashier <small>for {{$tformat[$t]}}</small></h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive"">
                <table class="table table-bordered">
                    <thead>
                    <th>Name</th>
                    <th>Total Amount</th>
                    </thead>
                    <tbody>
                    @foreach($cashier as $cashiers)
                        <tr>
                            <td><a href="{{url($role.'/dashboard?access='.$cashiers['id'].'&name='.$cashiers['name'])}}">{{$cashiers['name']}}</a></td>
                            <td>{{ $cashiers['total_sold'] }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
        {{ array_forget($products, ['overalltotal_today','overalltotal_collection','overalltotal','overalltotal_cash','overalltotal_credit', 'overalltotal_cash_discount', 'overalltotal_credit_discount', 'overalltotal_discount']) }}

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Sales by category <small>for {{$tformat[$t]}}</small></h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive"">
            <table class="table table-bordered">
                <thead>
                <th>Name</th>
                <th>Total Amount</th>
                </thead>
                <tbody>
                @foreach($products as $keys=>$vals)
                    <tr>
                        <td><a href="#{{ $keys }}">{{ $keys }}</a></td>
                        <td>{{($vals['total_cash_price'] + $vals['total_credit_price']) - $vals['total_discount_cash_credit']}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

</div>

<div class="box box-info" id="listOfTransaction">
    <div class="box-header with-border">
        <h3 class="box-title">List of transaction <small>for {{$tformat[$t]}}</small></h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body table-responsive"">
    <table class="table table-bordered">
        <thead>
        <th>Transaction#</th>
        <th>Customer</th>
        <th>Status</th>
        <th>Dscnt</th>
        <th>Total</th>
        {!! $role=='owner'||$role=='manager'?'<th></th>':'' !!}
        </thead>
        <?php $total_discount = 0; ?>
        <?php $countTrans = 0; ?>
        <?php $countCreditTrans = 0; ?>
        @foreach($transactions as $transaction)
            <?php $trans_total = $transaction['total_amount']; ?>
            <?php $trans_discount = $transaction['total_discount']; ?>
            <?php $total_discount += $trans_discount; ?>
            <?php $countTrans++; ?>
            @if($transaction[0]['remarks']==1)
            <?php $countCreditTrans++; ?>
            @endif
            {{ array_forget($transaction,['total_amount','total_discount']) }}
            <tr>
                <td><a class="trans-number" href="{{url($role.'/findtrans?q='.$transaction[0]['transaction_number'])}}">{{$transaction[0]['transaction_number']}}</a></td>
                <td>{{$transaction[0]['credit_info']}}</td>
                <td>{{$transaction[0]['remarks']==1?'Credit':'Paid'}}</td>
                <td>{{$trans_discount}}</td>
                <td class="trans-total">{{$trans_total}}</td>
                {!! $role=='owner'||$role=='manager'?'<td><a href="javascript:;" class="glyphicon glyphicon-remove-circle remove-trans"></a></td>':'' !!}
            </tr>
        @endforeach
        <input type="hidden" name="total-discount" class="total-discount" value="{{$total_discount}}">
        <input type="hidden" name="count-transaction" class="count-transaction" value="{{$countTrans}}">
        <input type="hidden" name="credit-transaction" class="credit-transaction" value="{{$countCreditTrans}}">
    </table>
</div>
</div>
</div>
{{--@if($role!='cashier')--}}
<div class="col-sm-7 col-xs-12">
    @foreach($products as $keys=>$vals)
        <div class="box box-success" id="{{ $keys }}">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $keys }} : (P) {{ ($vals['total_cash_price'] + $vals['total_credit_price']) - $vals['total_discount_cash_credit'] }}</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body no-padding table-responsive"">
            <table class="table table-condensed">
                <thead>
                <th style="width: 10px">#</th>
                <th>Items</th>
                <th class="text-center">Left</th>
                <th class="text-center">Cash</th>
                <th class="text-center">Amt</th>
                <th class="text-center">Credit</th>
                <th class="text-center">Amt</th>
                <th class="text-center">Dscnt</th>
                <th>Total</th>
                </thead>
                <tbody>
                {{ array_forget($vals, ['total_sold','total_cash_price','total_credit_price','total_qty_cash_credit', 'total_discount_cash_credit']) }}
                <?php $x=1; ?>
                @foreach($vals as $key=>$product)
                    @if($products[$keys]['total_sold'])
                        <?php $total_sold = (100 / $products[$keys]['total_sold']) * $product['sold_qty_total']; ?>
                    @else
                        <?php $total_sold = 0; ?>
                    @endif
                    <tr>
                        <td>{{ $x++ }}</td>
                        <td>{{ $product['description'] }}</td>
                        <td class="text-center">{{ $product['quantity'] }}</td>
                        <td class="text-center bg-primary">{{ $product['sold_qty_cash'].$product['unit'] }} </td>
                        <td class="text-center bg-primary">{{ $product['sold_total_cash'] }}</td>
                        <td class="text-center bg-info">{{ $product['sold_qty_credit'].$product['unit'] }}</td>
                        <td class="text-center bg-info">{{ $product['sold_total_credit'] }}</td>
                        <td class="text-center">{{ $product['sold_total_discount'] }}</td>
                        <td class="text-center">{{ ($product['sold_total_credit'] + $product['sold_total_cash']) - $product['sold_total_discount'] }}</td>
                    </tr>
                @endforeach
                </tbody></table>
        </div><!-- /.box-body -->
</div>
@endforeach
</div>
{{--@endif--}}
</div>
<div class="print-wrap" style="display:none; width:900px; margin:0 auto;">
    <div style="text-align:center;">
        <h3><em>BIGBYTE ENTERPRISES</em></h3>
        Poblacion, Siquijor, Siquijor &nbsp; Tel Nos. (035) 4809987; 4805513<br>
        Aliffod B. Espinosa - Prop. &nbsp; VAT Reg. TIN 182-525-761-000<br>
        Telefax (035) 4805515 &nbsp; Cell #. 09194112896</p>
        <strong>{{$t}} Report</strong><br><br>
    </div>
    <p style="text-align: right"><strong>Date</strong>: {{$r}}</p>
    <div>
        <div style="width:50%; float:left;">
            <table border="1" style="width:100%;">
                <thead>
                <th colspan="5" style="text-align: center;">SALES</th>
                </thead>
                <tbody>
                <tr>
                    <td style="text-align: center">No.</td>
                    <td style="text-align: center">Description</td>
                    <td style="text-align: center">Cash</td>
                    <td style="text-align: center">Crdt</td>
                    <td style="text-align: center">Collct</td>
                </tr>
                <?php $overalltotal_cash = $lists['overalltotal_cash'];  ?>
                <?php $overalltotal_today = $lists['overalltotal_today'];  ?>
                <?php $overalltotal_credit = $lists['overalltotal_credit'];  ?>
                <?php $overalltotal_collection = $lists['overalltotal_collection'];  ?>
                <?php $overalltotal_credit_discount = $lists['overalltotal_credit_discount'];  ?>
                <?php $overalltotal_cash_discount = $lists['overalltotal_cash_discount'];  ?>
                {{ array_forget($lists, ['overalltotal_today','overalltotal_collection','overalltotal_discount','overalltotal_credit_discount','overalltotal_cash_discount','overalltotal_credit','overalltotal_cash','overalltotal']) }}
                @foreach($lists as $items)
                    {{ array_forget($items, ['total_cash_price','total_credit_price','total_qty_cash_credit','total_sold','total_discount_cash_credit']) }}
                    @foreach($items as $key=>$item)
                        @if($item['sold_total_cash']!=0 || $item['sold_total_credit']!=0)
                            <tr>
                                <td style="text-align: center">{{$sa_no++}}</td>
                                <td>{{$item['description']}}</td>
                                <td style="text-align: center">{{$item['sold_total_cash'] - $item['sold_cash_discount']}}</td>
                                <td style="text-align: center">{{$item['sold_total_credit'] - $item['sold_credit_discount']}}</td>
                                <td></td>
                            </tr>
                        @endif
                    @endforeach
                @endforeach
                <tr>
                    <td colspan="2" style="text-align: center">Total</td>
                    <!--td style="text-align: center">{{--($overalltotal_cash + $overalltotal_collection) - $overalltotal_cash_discount--}}</td-->
                    <td style="text-align: center">{{$sebal}}</td>
                    <td style="text-align: center">{{($overalltotal_credit - $overalltotal_collection) - $overalltotal_credit_discount}}</td>
                    <td style="text-align: center">{{$overalltotal_collection}}</td>
                </tr>
                <tr><td colspan="5" style="text-align: center;">*********************************************************************</td></tr>
                </tbody>
            </table>
            <table border="1" style="width:100%;">
                <thead>
                <th colspan="5" style="text-align: center;">SERVICES</th>
                </thead>
                <tbody>
                <tr>
                    <td style="text-align: center">No.</td>
                    <td style="text-align: center">Description</td>
                    <td style="text-align: center">Cash</td>
                    <td style="text-align: center">Crdt</td>
                    <td style="text-align: center">Collct</td>
                </tr>
                <?php $se_cash = $ses['overall_cash']; ?>
                <?php $se_credit = $ses['overall_credit']; ?>
                <?php $se_collection = $ses['overall_collection']; ?>
                {{ array_forget($ses, ['overall_cash','overall_credit','overall_collection']) }}
                @foreach($ses as $customer=>$se)
                    <tr>
                        <td style="text-align: center">{{$se_no++}}</td>
                        <td>{{$customer}}{{$se['customer']!=''? ' - '. $se['customer']:''}}</td>
                        <td style="text-align: center">{{$se['cash']}}</td>
                        <td style="text-align: center">{{$se['credit'] - $se['collection']}}</td>
                        <td style="text-align: center">{{$se['collection']}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="2" style="text-align: center">Total</td>
                    <!--td style="text-align: center">{{--$se_cash + $se_collection--}}</td-->
                    <td style="text-align: center">{{$services['se']['cash']}}</td>
                    <td style="text-align: center">{{$se_credit - $se_collection}}</td>
                    <td style="text-align: center">{{$overalltotal_collection}}</td>
                </tr>
                <tr><td colspan="5" style="text-align: center;">*********************************************************************</td></tr>
                </tbody>
            </table>

            <table border="1" style="width:100%;">
                <thead>
                <th colspan="6" style="text-align: center;">PHOTOCOPY</th>
                </thead>
                <tbody>
                <tr>
                    <td style="text-align: center">No.</td>
                    <td>Customer</td>
                    <td style="text-align: center">Pcs</td>
                    <td style="text-align: center">Cash</td>
                    <td style="text-align: center">Crdt</td>
                    <td style="text-align: center">Collct</td>
                </tr>
                <?php $px_pcs = $pxs['overall_pcs']; ?>
                <?php $px_cash = $pxs['overall_cash']; ?>
                <?php $px_credit = $pxs['overall_credit']; ?>
                <?php $px_collection = $pxs['overall_collection']; ?>
                {{ array_forget($pxs, ['overall_pcs','overall_cash','overall_credit','overall_collection']) }}
                @foreach($pxs as $customer=>$px)
                    <tr>
                        <td style="text-align: center">{{$px_no++}}</td>
                        <td>{{$px['customer']}}</td>
                        <td style="text-align: center">{{$px['pcs']}}</td>
                        <td style="text-align: center">{{$px['remarks']!=1?$px['amount']:''}}</td>
                        <td style="text-align: center">{{$px['remarks']==1?$px['amount']:''}}</td>
                        <td style="text-align: center">{{$px['collection']}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="2" style="text-align: center">Total</td>
                    <td style="text-align: center">{{$px_pcs}}</td>
                    <!--td style="text-align: center">{{--$px_cash + $px_collection--}}</td-->
                    <td style="text-align: center">{{$services['px']['cash']}}</td>
                    <td style="text-align: center">{{$px_credit - $px_collection}}</td>
                    <td style="text-align: center">{{$px_collection}}</td>
                </tr>
                <tr><td colspan="6" style="text-align: center;">*********************************************************************</td></tr>
                </tbody>
            </table>
        </div>
        <div style="width:50%; float:left;">
            <table border="1" style="width:100%;">
                <thead>
                <th colspan="6" style="text-align: center;">AQUABYTE</th>
                </thead>
                <tbody>
                <tr>
                    <td style="text-align: center">No.</td>
                    <td style="text-align: center">Gal/s</td>
                    <td style="text-align: center">Customer</td>
                    <td style="text-align: center">Cash</td>
                    <td style="text-align: center">Crdt</td>
                    <td style="text-align: center">Collct</td>
                </tr>
                <?php $aq_cash = $aqs['overall_cash']; ?>
                <?php $aq_credit = $aqs['overall_credit']; ?>
                <?php $aq_collection = $aqs['overall_collection']; ?>
                {{ array_forget($aqs, ['overall_cash','overall_credit','overall_collection','overall_gals']) }}
                @foreach($aqs as $aq)
                    <tr>
                        <td style="text-align: center">{{$aq_no++}}</td>
                        <td style="text-align: center">{{$aq['gal']}}</td>
                        <td>{{$aq['customer']}}</td>
                        <td style="text-align: center">{{$aq['remarks']!=1?$aq['amount']:''}}</td>
                        <td style="text-align: center">{{$aq['remarks']==1?$aq['amount']:''}}</td>
                        <td style="text-align: center">{{$aq['collection']}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="3" style="text-align: center">Total</td>
                    <td style="text-align: center">{{$services['aq']['cash']}}</td>
                    <!--td style="text-align: center">{{--$aq_cash--}}</td-->
                    <td style="text-align: center">{{$aq_credit}}</td>
                    <td style="text-align: center">{{$aq_collection}}</td>
                </tr>
                <tr><td colspan="6" style="text-align: center;">*********************************************************************</td></tr>
                </tbody>
            </table>


            <table border="1" style="width:100%;">
                <thead>
                <th colspan="6" style="text-align: center;">E-LOAD</th>
                </thead>
                <tbody>
                <tr>
                    <td style="text-align: center">No.</td>
                    <td style="text-align: center">Description</td>
                    <td style="text-align: center">Load</td>
                    <td style="text-align: center">Cash</td>
                    <td style="text-align: center">Crdt</td>
                    <td style="text-align: center">Collct</td>
                </tr>
                <?php $el_load = $els['overall_load']; ?>
                <?php $el_cash = $els['overall_cash']; ?>
                <?php $el_credit = $els['overall_credit']; ?>
                <?php $el_collection = $els['overall_collection']; ?>
                {{ array_forget($els, ['overall_cash','overall_credit','overall_collection','overall_load']) }}
                @foreach($els as $el)
                    <tr>
                        <td style="text-align: center">{{$el_no++}}</td>
                        <td style="text-align: center">{{$el['network'].' '.$el['customer']}}</td>
                        <td style="text-align: center">{{$el['load']}}</td>
                        <td style="text-align: center">{{$el['remarks']!=1?$el['amount']:''}}</td>
                        <td style="text-align: center">{{$el['remarks']==1?$el['amount']:''}}</td>
                        <td style="text-align: center">{{$el['collection']}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="2" style="text-align: center">Total</td>
                    <td style="text-align: center">{{$el_load}}</td>
                    <!--td style="text-align: center">{{--$el_cash + $el_collection--}}</td-->
                    <td style="text-align: center">{{$services['el']['cash']}}</td>
                    <td style="text-align: center">{{$el_credit - $el_collection}}</td>
                    <td style="text-align: center">{{$el_collection}}</td>
                </tr>
                <tr><td colspan="6" style="text-align: center;">*********************************************************************</td></tr>
                </tbody>
            </table>

            <table border="1" style="width:100%;">
                <thead>
                <th colspan="7" style="text-align: center;">HANDS-ON</th>
                </thead>
                <tbody>
                <tr>
                    <td style="text-align: center">No.</td>
                    <td style="text-align: center">Customer</td>
                    <td style="text-align: center">Time-in</td>
                    <td style="text-align: center">Time-out</td>
                    <td style="text-align: center">Cash</td>
                    <td style="text-align: center">Crdt</td>
                    <td style="text-align: center">Collct</td>
                </tr>
                <?php $ho_cash = $hos['overall_cash']; ?>
                <?php $ho_credit = $hos['overall_credit']; ?>
                <?php $ho_collection = $hos['overall_collection']; ?>
                {{ array_forget($hos, ['overall_cash','overall_credit','overall_collection']) }}
                @foreach($hos as $ho)
                    <tr>
                        <td style="text-align: center">{{$ho_no++}}</td>
                        <td style="text-align: center">{{$ho['customer']}}</td>
                        <td style="text-align: center">{{$ho['time_in']->format('h:i a')}}</td>
                        <td style="text-align: center">{{$ho['time_out']->format('h:i a')}}</td>
                        <td style="text-align: center">{{$ho['remarks']!=1?$ho['amount']:''}}</td>
                        <td style="text-align: center">{{$ho['remarks']==1?$ho['amount']:''}}</td>
                        <td style="text-align: center">{{$ho['collection']}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="3" style="text-align: center">Total</td>
                    <td></td>
                    <!--td style="text-align: center">{{--$ho_cash + $ho_collection--}}</td-->
                    <td style="text-align: center">{{$services['ho']['cash']}}</td>
                    <td style="text-align: center">{{$ho_credit - $ho_collection}}</td>
                    <td style="text-align: center">{{$ho_collection}}</td>
                </tr>
                <tr><td colspan="7" style="text-align: center;">*********************************************************************</td></tr>
                </tbody>
            </table>

            <?php $summary_cash = 0; ?>
            <?php $summary_credit = 0; ?>
            <?php $summary_collection = 0; ?>
            <table border="1" style="width:100%;">
                <thead>
                <th colspan="7" style="text-align: center;">SUMMARY</th>
                </thead>
                <tbody>
                <tr>
                    <td style="text-align: center">No.</td>
                    <td style="text-align: center">Name</td>
                    <td style="text-align: center">Cash</td>
                    <td style="text-align: center">Crdt</td>
                    <td style="text-align: center">Collct</td>
                </tr>
                <tr>
                    <td style="text-align: center;">{{$su_no++}}</td>
                    <td>Sales</td>
                    <!--td style="text-align: center;">{{-- $summary_cash += ($overalltotal_cash - $overalltotal_cash_discount) + $overalltotal_collection --}}</td-->
                    <td style="text-align: center;">{{$summary_cash += $sebal}}</td>
                    <td style="text-align: center;">{{ $summary_credit += $overalltotal_credit - ($overalltotal_credit_discount + $overalltotal_collection) }}</td>
                    <td style="text-align: center;">{{ $summary_collection += $overalltotal_collection}}</td>
                </tr>

                @foreach($services as $service)
                    <?php $summary_cash += $service['cash']; ?>
                    <?php $summary_credit += $service['credit'] - $service['collection']; ?>
                    <?php $summary_collection += $service['collection'] ?>
                    <tr>
                        <td style="text-align: center;">{{$su_no++}}</td>
                        <td>{{$service['desc']}}</td>
                        <!--td style="text-align: center;">{{--$service['amount'] + $service['collection']--}}</td-->
                        <td style="text-align: center;">{{$service['cash']}}</td>
                        <td style="text-align: center;">{{$service['credit'] - $service['collection']}}</td>
                        <td style="text-align: center;">{{$service['collection']}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="2" style="text-align: center">Total</td>
                    <td style="text-align: center" class="total-cash">{{$summary_cash}}</td>
                    <td style="text-align: center" class="total-credit">{{$summary_credit}}</td>
                    <td style="text-align: center" class="total-collection">{{$summary_collection}}</td>
                </tr>
                <tr><td colspan="7" style="text-align: center;">*********************************************************************</td></tr>
                </tbody>
            </table>
        </div>

        <div class="clearfix"></div>

    </div>
</div>
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
@include('partials.footer')
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready( function (){
            var numOfTransHtml = '<span class="progress-text">Number of Transaction</span><span class="progress-number"><b>',
                numOfCreditTransHtml = '<span class="progress-text">Credit Transaction</span><span class="progress-number"><b>',
                numOfTrans=parseFloat($('.count-transaction').val()),
                numOfCreditTrans=parseFloat($('.credit-transaction').val());

            numOfTransHtml += numOfTrans;
            numOfTransHtml += '</b>/';
            numOfTransHtml += numOfTrans;
            numOfTransHtml += '</span><div class="progress sm"><div class="progress-bar progress-bar-red" style="width: ';
            numOfTransHtml += (numOfTrans/numOfTrans)*100;
            numOfTransHtml += '%"></div></div>';

            numOfCreditTransHtml += numOfCreditTrans;
            numOfCreditTransHtml += '</b>/';
            numOfCreditTransHtml += numOfTrans;
            numOfCreditTransHtml += '</span><div class="progress sm"><div class="progress-bar progress-bar-yellow" style="width: ';
            numOfCreditTransHtml += (numOfCreditTrans/numOfTrans)*100;
            numOfCreditTransHtml += '%"></div></div>';

            $('.number-of-transaction').html(numOfTransHtml);
            $('.number-of-credit-transaction').html(numOfCreditTransHtml);

            var v = '{{$r}}';

            $('.select-yr').on('change', function() {
                if($('.select-mo').length&&!$('.select-dy').length) { v = $('.select-yr').val() + '-' + $('.select-mo').val(); $('.r').val(v); }
                if($('.select-mo').length&&$('.select-dy').length) { v = $('.select-yr').val() + '-' + $('.select-mo').val() + '-' + $('.select-dy').val(); $('.r').val(v); }
                if(!$('.select-mo').length&&!$('.select-dy').length) { v = $('.select-yr').val(); $('.r').val(v); }
            });

            $('.select-mo').on('change', function() {
                if($('.select-mo').length&&!$('.select-dy').length) { v = $('.select-yr').val() + '-' + $('.select-mo').val(); $('.r').val(v); }
                if($('.select-mo').length&&$('.select-dy').length) { v = $('.select-yr').val() + '-' + $('.select-mo').val() + '-' + $('.select-dy').val(); $('.r').val(v); }
                if(!$('.select-mo').length&&!$('.select-dy').length) { v = $('.select-yr').val(); $('.r').val(v); }
            });

            $('.select-dy').on('change', function() {
                if($('.select-mo').length&&!$('.select-dy').length) { v = $('.select-yr').val() + '-' + $('.select-mo').val(); $('.r').val(v); }
                if($('.select-mo').length&&$('.select-dy').length) { v = $('.select-yr').val() + '-' + $('.select-mo').val() + '-' + $('.select-dy').val(); $('.r').val(v); }
                if(!$('.select-mo').length&&!$('.select-dy').length) { v = $('.select-yr').val(); $('.r').val(v); }
            });

            $('.print-btn').on('click', function() {

                var mywindow = window.open('', '','left=300, top=100, width=900, height=500, toolbar=0, scrollbars=0, status=0');

                var htmlcontent = '<!DOCTYPE html>';
                htmlcontent += '<html><head><style> html,body { font-family:sans-serif; margin:0 auto; font-size: 9pt; } </style> </head><body>';
                htmlcontent += $('.print-wrap').html();
                htmlcontent += '</body></html>';

                mywindow.document.write(htmlcontent);
                mywindow.document.close();
                mywindow.focus();
                mywindow.print();
                mywindow.close();

            });

            $('.remove-trans').on('click', function() {
                var trans = $(this).parents('tr').find('.trans-number').text();
                if(confirm('Remove transaction?')){
                    $.ajax({
                        url: '{{url('owner/remove-trans')}}',
                        type: 'post',
                        data: { trans: trans, date: '{{$r}}', _token: '{{ csrf_token() }}' },
                        success: function(response) {
                            window.location.reload();
                            //console.log(response);
                        },
                        error: function() {
                            alert('Whoops, something went wrong in removing transaction.');
                        }
                    });
                }

            });

            $('.pay-balance').on('click', function() {
                var credit_slug = $(this).parents('td').find('.credit-slug').text(),
                        credit_balance = $(this).parents('tr').find('.balance-amount').text(),
                        actual_balance = $(this).parents('tr').find('.actual-amount').val(),
                        credit_name = $(this).parents('td').find('.credit-name').text();
                if(confirm('This will remove remaining credit balance for '+credit_name+'. Continue?')) {
                    $.ajax({
                        url: '{{url($role.'/paybalance')}}',
                        type: 'post',
                        data: {_token: '{{ csrf_token() }}', name: credit_name, balance: credit_balance, actual: actual_balance },
                        success: function (response) {
                            $('.'+credit_slug).hide();
                        },
                        error: function () {
                            alert('Whoops, pay balance request looks like something went wrong.');
                        }
                    });
                }

            });

        });
    </script>
    <script src="{{ url('assets/plugins/chartjs/Chart.min.js') }}"></script>
    <script>
        $(function () {
            /* ChartJS
             * -------
             * Here we will create a few charts using ChartJS
             */

            // Get context with jQuery - using jQuery's .get() method.
            var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
            // This will get the first returned node in the jQuery collection.
            var areaChart = new Chart(areaChartCanvas);

            var areaChartData = {
                labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                datasets: [
                    {
                        label: "Sale",
                        fillColor: "#f56954",
                        strokeColor: "#f56954",
                        pointColor: "#f56954",
                        pointStrokeColor: "#f56954",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [
                            @foreach($cashPerMonth['sa'] as $monthlyCash)
                                {{$monthlyCash}},
                            @endforeach
                            ]
                    },
                    @foreach($services as $service)
                    {
                        label: "{{$service['desc']}}",
                        fillColor: "{{$service['asci']}}",
                        strokeColor: "{{$service['asci']}}",
                        pointColor: "{{$service['asci']}}",
                        pointStrokeColor: "{{$service['asci']}}",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [
                        @foreach($cashPerMonth[$service['short']] as $monthlyCash)
                            {{$monthlyCash}},
                        @endforeach
                        ]
                    },
                    @endforeach
                ]
            };

            var areaChartOptions = {
                //Boolean - If we should show the scale at all
                showScale: true,
                //Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines: false,
                //String - Colour of the grid lines
                scaleGridLineColor: "rgba(0,0,0,.05)",
                //Number - Width of the grid lines
                scaleGridLineWidth: 1,
                //Boolean - Whether to show horizontal lines (except X axis)
                scaleShowHorizontalLines: true,
                //Boolean - Whether to show vertical lines (except Y axis)
                scaleShowVerticalLines: true,
                //Boolean - Whether the line is curved between points
                bezierCurve: true,
                //Number - Tension of the bezier curve between points
                bezierCurveTension: 0.3,
                //Boolean - Whether to show a dot for each point
                pointDot: true,
                //Number - Radius of each point dot in pixels
                pointDotRadius: 3,
                //Number - Pixel width of point dot stroke
                pointDotStrokeWidth: 2,
                //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                pointHitDetectionRadius: 20,
                //Boolean - Whether to show a stroke for datasets
                datasetStroke: true,
                //Number - Pixel width of dataset stroke
                datasetStrokeWidth: 2,
                //Boolean - Whether to fill the dataset with a color
                datasetFill: false,
                //String - A legend template
                legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
                //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio: true,
                //Boolean - whether to make the chart responsive to window resizing
                responsive: true
            };

            //Create the line chart
            areaChart.Line(areaChartData, areaChartOptions);

            //-------------
            //- PIE CHART -
            //-------------
            // Get context with jQuery - using jQuery's .get() method.
            var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
            var pieChart = new Chart(pieChartCanvas);
            var PieData = [
                @foreach($services as $service)
                {
                    value: "{{$service['cash']}}",
                    color: "{{$service['asci']}}",
                    highlight: "{{$service['asci']}}",
                    label: "{{$service['desc']}}"
                },
                @endforeach
                {
                    value: "{{$sebal}}",
                    color: "#f56954",
                    highlight: "#f56954",
                    label: "Sale"
                }
            ];
            var pieOptions = {
                //Boolean - Whether we should show a stroke on each segment
                segmentShowStroke: true,
                //String - The colour of each segment stroke
                segmentStrokeColor: "#fff",
                //Number - The width of each segment stroke
                segmentStrokeWidth: 2,
                //Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 50, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps: 100,
                //String - Animation easing effect
                animationEasing: "easeOutBounce",
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate: true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale: true,
                //Boolean - whether to make the chart responsive to window resizing
                responsive: true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio: true,
                //String - A legend template
                legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
            };
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
            pieChart.Doughnut(PieData, pieOptions);


        });

        Number.prototype.formatMoney = function(decPlaces, thouSeparator, decSeparator) {
            var n = this,
                    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
                    decSeparator = decSeparator == undefined ? "." : decSeparator,
                    thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
                    sign = n < 0 ? "-" : "",
                    i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
                    j = (j = i.length) > 3 ? j % 3 : 0;
            return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
        };

        var totalCash = parseFloat($('.total-cash').text()),
            totalCredit = parseFloat($('.total-credit').text()),
            totalCollection = parseFloat($('.total-collection').text()),
            totalDiscount = parseFloat($('.total-discount').val());
        $('.total-cash-amount').text(totalCash.formatMoney(2, ',', '.'));
        $('.total-credit-amount').text(totalCredit.formatMoney(2, ',', '.'));
        $('.total-collection-amount').text(totalCollection.formatMoney(2, ',', '.'));
        $('.total-discount-amount').text(totalDiscount.formatMoney(2, ',', '.'));
    </script>

@endsection