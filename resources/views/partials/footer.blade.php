<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> {{ config('app-settings.version') }}
    </div>
    <strong>Copyright &copy; {{ date('Y') }} <a href="{{ config('app-settings.website') }}">{{ config('app-settings.company') }}</a>.</strong> All rights reserved.
</footer>