@extends('layouts.master',['title'=>'Transaction'])
@section('title','Transaction')

@section('content')

@include('partials.nav',['role'=>$role, 'active'=>''])
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Transaction
            <small># <span class="transaction">{{ $trans }}</span></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title credit-by">{{ $sold['remarks']==1 ? 'Credit / '.$sold['credit_info'] : 'Cash'}}</h3>
                <p class="box-title" style="float:right;"><small>{{$sold['date']}}</small></p>

            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead class="bg-primary">
                        <th>Description</th>
                        <th>Qty</th>
                        <th>Unit</th>
                        <th>Price</th>
                        <th>Discount</th>
                        <th>Amount</th>
                        </thead>
                        <tbody>
                        <?php $remarks = $sold['remarks']; ?>
                        <?php $total_discount = 0; ?>
                        <?php $total_amount = 0; ?>
                        <?php $partial_payment = $sold['partial_payment']; ?>
                        {{array_forget($sold,['remarks','credit_info','date','partial_payment'])}}
                        @foreach($sold as $key=>$sales)
                            <tr>
                                <td>{{$sales['description']}}</td>
                                <td>{{$sales['quantity']}}</td>
                                <td>{{$sales['unit']}}</td>
                                <td>{{$sales['price']}}</td>
                                <td>{{$sales['discount']}}</td>
                                <td>{{$sales['amount']}}</td>
                                <?php $total_discount += $sales['discount']; ?>
                                <?php $total_amount += $sales['amount']; ?>
                            </tr>
                        @endforeach
                        @if($remarks==1)
                            <tr class="collection-row bg-primary">
                                <td>
                                    <input style="color:black; padding:1px 4px;" type="text" name="install" class="add-install" placeholder=" Installment">
                                    <button class="btn btn-danger btn-xs add-install-btn btn-flat">Add</button>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>Collection</td>
                                <td colspan="3" class="partial-payment">{{$partial_payment}}</td>
                            </tr>
                        @endif
                        <tr class="bg-primary">
                            <td>{!! $remarks==1?'<button class="btn btn-default btn-xs pay-now btn-flat">Pay now</button>':'' !!}
                                <button class="btn btn-warning btn-xs btn-flat print">Print</button>
                                <a href="{{ redirect()->back()->getTargetUrl() }}#listOfTransaction" class="btn btn-warning btn-xs btn-flat btn-back">Back</a>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Total</td>
                            <input type="hidden" class="actual-remaining" name="actual-remaining" value="{{$total_amount - $partial_payment}}">
                            <td class="credit-remaining" contenteditable="true">{{$total_amount - $partial_payment}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

@include('partials.footer')

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready( function (){
            $('.pay-now').on('click', function() {
                var collect = parseFloat($('.partial-payment').text()),
                    actualremaining = parseFloat($('.actual-remaining').val()),
                    remaining = parseFloat($('.credit-remaining').text()),
                    total = collect + remaining;
                $.ajax({
                    url : '{{url($role.'/paynow')}}',
                    type : 'post',
                    data : { _token : '{{ csrf_token() }}', transaction:$('.transaction').text(), amount: remaining, actual: actualremaining },
                    success : function(response) {
                        $('.credit-remaining').text(total);
                    },
                    error : function() {
                        alert('Whoops, pay now request looks like something went wrong.');
                    }
                });
                $(this).hide();
                $('.collection-row').hide();
                $('.credit-by').text('Paid');
            });

            $('.add-install').on('keyup', function() {
                //console.log($(this).val());
                var installment = parseFloat($(this).val()),
                        remaining = parseFloat($('.credit-remaining').text());
                if(installment>remaining) {
                    $(this).val(remaining);
                }
            });

            $('.add-install-btn').on('click', function() {
                var installment = parseFloat($('.add-install').val()),
                        partial = parseFloat($('.partial-payment').text()),
                        remaining = parseFloat($('.credit-remaining').text()),
                        total = installment+partial;
                if(installment<remaining) {
                    $.ajax({
                        url : '{{url($role.'/addinstallment')}}',
                        type : 'post',
                        data : { _token : '{{ csrf_token() }}', installment:total, cash:installment, transaction:$('.transaction').text() },
                        success : function(response) {
                            console.log(response);
                            $('.credit-remaining').text(remaining-installment);
                            $('.add-install').val('');
                            $('.partial-payment').text(total);
                        },
                        error : function() {
                            alert('Whoops, install amount failed.');
                        }
                    });
                }
                else { alert('Use pay now button.'); }
            });

            $('.print').on('click', function() {
                var mywindow = window.open('', '','left=300, top=100, width=800, height=500, toolbar=0, scrollbars=0, status=0');

                var htmlcontent = '<!DOCTYPE html>';
                htmlcontent += '<html><head> <style> .add-install,.add-install-btn,.btn-back,.print,.pay-now{ display:none; } body,table{ max-width:700px; width:100%; margin:0 auto; font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; } td{ text-align:center; }</style> </head><body>';
                htmlcontent += $('.content-wrapper').html();
                htmlcontent += '</body></html>';

                mywindow.document.write(htmlcontent);
                mywindow.document.close();
                mywindow.focus();
                mywindow.print();
                mywindow.close();
            });

        });
    </script>
@endsection