@extends('layouts.master')
@section('title','Manage User')

@section('content')
@include('partials.nav',['role'=>$role,'active'=>'manage-user'])

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Manage Users
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Manage</a></li>
            <li class="active">Users</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        @if($role=='admin')
        <div class="row add-cashier-form">
            <form action="{{ url($role.'/manage-user/') }}" method="post">
                {!! csrf_field() !!}
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title"><small>Create New User</small></h3>
                            <div class="box-tools">
                                <button type="submit" class="btn btn-success btn-block btn-flat cashier-save">Save <span class="glyphicon glyphicon-ok"></span></button>
                            </div>
                            <div class="box-tools" style="right:90px;">
                                <button type="reset" class="btn btn-warning btn-block btn-flat add-cashier-close">Close <span class="glyphicon glyphicon-remove"></span></button>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Password</th>
                                    <th>Role</th>
                                </tr>
                                <tr>
                                    <td><input type="text" class="form-control cashier-value" name="name" style="width:220px;"></td>
                                    <td><input type="text" class="form-control cashier-value" name="email" style="width:220px;"></td>
                                    <td><input type="text" class="form-control cashier-value" name="password" style="width:220px;"></td>
                                    <td>
                                        <select class="form-control cashier-value" name="role">
                                            @foreach(config('app-settings.role') as $userRole)
                                            <option value="{{ $userRole }}">{{ ucfirst($userRole) }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
            </form>
        </div>
        @endif
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Users</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>New Password</th>
                                    {!! $role=='admin'||$role=='owner'?'<th>Role</th>':'' !!}
                                    <th></th>
                                </tr> <?php $x=1; ?>
                                @foreach($cashiers as $cashier)
                                    <tr>
                                        <td>{{ $x++ }}</td>
                                        <input type="hidden" name="id" class="cashier-id" value="{{ $cashier['id'] }}">
                                        <td><input class="cashier-info no-style-textbox cashier-name" type="text" name="name" value="{{ $cashier['name'] }}" style="width:150px; padding-left:5px;"></td>
                                        <td><input class="cashier-info no-style-textbox cashier-email" type="text" name="email" value="{{ $cashier['email'] }}" style="width:220px; padding-left:5px;"></td>
                                        <td><input class="cashier-info no-style-textbox cashier-password" type="text" name="password" placeholder="New Password" style="width:120px; padding-left:5px;"></td>
                                        {!! $role=='admin'||$role=='owner'?'<td>'.ucfirst($cashier['role']).'</td>':'' !!}
                                        <td>
                                            <a href="javascript:;" class="glyphicon glyphicon-pencil cashier-patch-btn"></a>
                                        </td>
                                        @if($role=='admin')
                                        <td>
                                            <a href="javascript:;" class="glyphicon glyphicon-remove cashier-destroy-btn"></a>
                                        </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
            </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

@include('partials.footer')

<!-- Control Sidebar -->
@include('partials.control')
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready( function (){
            $('.add-cashier-btn').on('click', function (){
                $('.add-cashier-form').show(150);
            });
            $('.add-cashier-close').on('click', function (){
                $('.add-cashier-form').hide(150);
            });

            $('.cashier-patch-btn').on('click', function (){
                var cashier = $(this).parents('tr');

                $.ajax({
                    url : '{{ url($role."/manage-user") }}/' + cashier.find('.cashier-id').val(),
                    type : 'POST',
                    data : {
                        '_token':'{{ csrf_token() }}',
                        '_method':'PATCH',
                        //'id':cashier.find('.cashier-id').val(),
                        'name':cashier.find('.cashier-name').val(),
                        'email':cashier.find('.cashier-email').val(),
                        'password':cashier.find('.cashier-password').val(),
                    },
                    success : function (response){
                        console.log(response);
                        cashier.find('.cashier-info').css('color','green');
                    },
                    error : function (){
                        cashier.find('.cashier-info').css('color','red');
                        alert('Whoops, looks like something went wrong.');
                    }
                });
            });

            $('.cashier-destroy-btn').on('click', function (){
                var cashier = $(this).parents('tr');

                if(confirm('Remove this user?')) {
                    $.ajax({
                         url : '{{ url($role."/manage-user") }}/' + cashier.find('.cashier-id').val(),
                         type : 'POST',
                         data : {
                         '_token':'{{ csrf_token() }}',
                         '_method':'DELETE'
                         },
                         success : function (response){
                             console.log(response);
                             cashier.remove();
                         },
                         error : function (){
                             cashier.find('.cashier-info').css('color','red');
                             alert('Whoops, looks like something went wrong.');
                         }
                    });
                }

            });

        });
    </script>
@endsection