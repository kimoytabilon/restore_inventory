@extends('layouts.master')

@section('content')	
	<!-- resources/views/auth/register.blade.php -->

	<h1>Register New User</h1>	
	<form method="POST" action="{{ url('/admin/register') }}">
		{!! csrf_field() !!}

		<div>
			Name
			<input type="text" name="name" value="{{ old('name') }}" class="form-control">
		</div>

		<div>
			Email
			<input type="email" name="email" value="{{ old('email') }}" class="form-control">
		</div>

		<div>
			Password
			<input type="password" name="password" class="form-control">
		</div>

		<div>
			Confirm Password
			<input type="password" name="password_confirmation" class="form-control">
		</div>
		<br>
		<div>
			<button type="submit" class="btn btn-success">Register</button>
		</div>
	</form>
@endsection