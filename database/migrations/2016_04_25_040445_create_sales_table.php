<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
			$table->increments('id');
			$table->string('transaction_number', 40);
			$table->integer('product_id');
			$table->boolean('quantity');
            $table->double('price');
            $table->double('partial_payment');
			$table->integer('user_id');
			$table->boolean('remarks');
			$table->string('credit_info');
            $table->double('discount');
            $table->double('total_amount');
            $table->double('cash_render');
            $table->double('total_change');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sales');
    }
}
