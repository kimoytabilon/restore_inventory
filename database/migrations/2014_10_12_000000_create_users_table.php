<?php

use App\User;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('img');
            $table->string('role',10);
            $table->rememberToken();
            $table->timestamps();
        });
        User::create(['name'=>'Kim Tabilon','email'=>'kim@kiakaha.co','password'=>bcrypt('admin123!'),'img'=>'user1-128x128.jpg','role'=>'admin']);
        User::create(['name'=>'Ian Reil Canto','email'=>'ian@kiakaha.co','password'=>bcrypt('ian_password'),'img'=>'user8-128x128.jpg','role'=>'admin']);
        User::create(['name'=>'Vicente Abana','email'=>'vic@kiakaha.co','password'=>bcrypt('vic_password'),'img'=>'user6-128x128.jpg','role'=>'manager']);
        User::create(['name'=>'Marion L O. Funtila','email'=>'marion@kiakaha.co','password'=>bcrypt('marion_password'),'img'=>'user2-160x160.jpg','role'=>'owner']);
        User::create(['name'=>'Jebert J. Elesterio','email'=>'jeb@kiakaha.co','password'=>bcrypt('jeb_password'),'img'=>'user8-128x128.jpg','role'=>'cashier']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
