<?php

use App\Product;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
			$table->increments('id');
			$table->string('code');
			$table->string('name');
			$table->text('description');
			$table->integer('quantity');
			$table->string('unit');
			$table->double('original_price');
			$table->double('cash_price');
			$table->double('credit_price');
			$table->integer('created_by');
			$table->integer('updated_by');
			$table->timestamps();
		});
		Product::create(['code'=>'','name'=>'Paper','description'=>'Copy Laser 8.5x11 Sub 20','quantity'=>100,'unit'=>'ream', 'original_price'=>20, 'cash_price'=>20, 'credit_price'=>20]);
		Product::create(['code'=>'','name'=>'Ballpen','description'=>'Panda','quantity'=>100,'unit'=>'pc', 'original_price'=>11, 'cash_price'=>12, 'credit_price'=>13]);
		Product::create(['code'=>'','name'=>'Ballpen','description'=>'GTech','quantity'=>100,'unit'=>'pc', 'original_price'=>35, 'cash_price'=>36, 'credit_price'=>37]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
