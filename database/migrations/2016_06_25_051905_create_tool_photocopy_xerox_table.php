<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToolPhotocopyXeroxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tool_photocopy_xerox', function($table) {
            $table->increments('id');
            $table->integer('user_id');

            $table->double('quantity');
            $table->double('price');
            $table->double('discount');
            $table->double('amount');

            $table->string('customer');
            $table->double('collection');

            $table->string('transaction_number');
            $table->boolean('remarks');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tool_photocopy_xerox');
    }
}
