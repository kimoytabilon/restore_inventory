## INSTALLATION GUIDE
	1. Install xampp-win32-5.6.8-0-VC11-installer.exe
	2. Install Composer-Setup.exe
	3. Open XAMPP Control Panel.exe and start both Apache and MySQL
	4. Open .env file inside kiakaha.ipos for the configuration and copy DB_DATABASE name
	5. Browse link localhost/phpmyadmin and create new database using the copied database name
	6. Open a terminal and browse the kiakaha.ipos folder
	7. Run `php artisan migrate`
	8. Run `php artisan serve` and browse localhost:8000
	9. Login Account:
			Admin:   ian@kiakaha.co,    ian_password
			Owner:   marion@kiakaha.co, marion_password
			Manager: vic@kiakaha.co,    vic_password
			Cashier: jeb@kiakaha.co,    jeb_password
	