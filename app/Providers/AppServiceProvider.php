<?php

namespace App\Providers;

use Auth;
use App\Product;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('partials.nav', function ($view){
            $view->with(['profile'=>Auth::user(),'zero_item'=>Product::where('quantity','<',1)->count()]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
