<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ToolPx extends Model
{
    protected $table = 'tool_photocopy_xerox';

    protected $fillable = [
        'user_id',
        'transaction_number',
        'customer',
        'quantity',
        'amount',
        'price',
        'discount',
        'remarks',
        'collection',
        'created_at',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @param $date
     */
    public function setCreatedAtAttribute($date)
    {
        $this->attributes['created_at'] = \Carbon\Carbon::parse($date);
    }
}
