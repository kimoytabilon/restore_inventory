<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = [
		'transaction_number',
		'product_id',
		'quantity',
		'price',
		'partial_payment',
		'user_id',
		'remarks',
		'credit_info',
		'discount',
		'total_amount',
		'cash_render',
		'total_change',
	];
	
	public function product()
	{
		return $this->belongsTo('App\Product');
	}
	
	public function user()
	{
		return $this->belongsTo('App\User');
	}

	/**
	 * @param $date
	 */
	public function setCreatedAtAttribute($date)
	{
		$this->attributes['created_at'] = \Carbon\Carbon::parse($date);
	}
}
