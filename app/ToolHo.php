<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ToolHo extends Model
{
    protected $table = 'tool_hands_on';

    protected $fillable = [
        'user_id',
        'transaction_number',
        'customer',
        'amount',
        'quantity',
        'price',
        'discount',
        'remarks',
        'collection',
        'time_in',
        'time_out',
        'created_at',
        'updated_at',
    ];

    protected $dates = ['time_in', 'time_out'];

    public function setTimeInAttribute($date)
    {
        $this->attributes['time_in'] = \Carbon\Carbon::parse($date);
    }

    public function setTimeOutAttribute($date)
    {
        $this->attributes['time_out'] = \Carbon\Carbon::parse($date);
    }

    public function setCreatedAtAttribute($date)
    {
        $this->attributes['created_at'] = \Carbon\Carbon::parse($date);
    }
}
