<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Authentication routes...
Route::get('/', 'Auth\AuthController@getLogin');
Route::post('/login', 'Auth\AuthController@postLogin');
Route::get('/logout', 'Auth\AuthController@getLogout');
Route::get('/lockscreen', 'UserController@lockscreen');

Route::group(['middleware'=>'auth'], function (){

    //CASHIER GROUP
    Route::group(['middleware'=>'role:cashier','prefix'=>'cashier'], function (){
        Route::resource('/dashboard', 'LogController');
        Route::resource('/sale', 'CashierController');
        Route::get('/account', 'UserController@account');
        Route::post('/update-account', 'UserController@account_update');
        Route::get('/findtrans', 'SaleController@findtrans');
    });

    // ADMIN GROUP
    Route::group(['middleware'=>'role:admin','prefix'=>'admin'], function (){
        Route::resource('/dashboard', 'LogController');
        Route::resource('/manage-user', 'UserController');
        Route::get('/account', 'UserController@account');
        Route::post('/update-account', 'UserController@account_update');
    });

    //MANAGER GROUP
    Route::group(['middleware'=>'role:manager','prefix'=>'manager'], function (){
        Route::resource('/dashboard', 'LogController');
        Route::resource('/manage-user', 'UserController');
        Route::resource('/manage-product', 'ProductController');
        Route::resource('/manage-sale', 'SaleController');

        Route::get('/findtrans', 'SaleController@findtrans');
        Route::post('/paynow', 'SaleController@paynow');
        Route::post('/paybalance', 'SaleController@paybalance');
        Route::post('/addinstallment', 'SaleController@addinstallment');
        Route::get('/account', 'UserController@account');
        Route::post('/update-account', 'UserController@account_update');
        Route::post('/remove-trans', 'SaleController@removeTrans');

        /* PHOTOCOPY/XEROX TOOL */
        Route::get('/photocopy-xerox/{type?}/{when?}', 'CashierController@photocopy_xerox');
        Route::post('/photocopy-xerox', 'CashierController@photocopy_xerox_store');
        Route::post('/px-paycredit', 'CashierController@px_paycredit');
        Route::post('/px-paybalance', 'CashierController@px_paybalance');
        Route::post('/px-destroy', 'CashierController@px_destroy');
        Route::post('/px-install', 'CashierController@px_install');

        /* AQUABYTE TOOL */
        Route::get('/aquabyte/{type?}/{when?}', 'CashierController@aquabyte');
        Route::post('/aquabyte', 'CashierController@aquabyte_store');
        Route::post('/aq-paycredit', 'CashierController@aq_paycredit');
        Route::post('/aq-paybalance', 'CashierController@aq_paybalance');
        Route::post('/aq-destroy', 'CashierController@aq_destroy');
        Route::post('/aq-install', 'CashierController@aq_install');

        /* E-LOAD TOOL */
        Route::get('/e-load/{type?}/{when?}', 'CashierController@eload');
        Route::post('/e-load', 'CashierController@eload_store');
        Route::post('/el-paycredit', 'CashierController@el_paycredit');
        Route::post('/el-paybalance', 'CashierController@el_paybalance');
        Route::post('/el-destroy', 'CashierController@el_destroy');
        Route::post('/el-install', 'CashierController@el_install');

        /* HANDS-ON TOOL */
        Route::get('/hands-on/{type?}/{when?}', 'CashierController@hands_on');
        Route::post('/hands-on', 'CashierController@hands_on_store');
        Route::post('/ho-paycredit', 'CashierController@ho_paycredit');
        Route::post('/ho-paybalance', 'CashierController@ho_paybalance');
        Route::post('/ho-destroy', 'CashierController@ho_destroy');
        Route::post('/ho-install', 'CashierController@ho_install');

        /* SERVICES TOOL */
        Route::get('/service/{type?}/{when?}', 'CashierController@service');
        Route::post('/service', 'CashierController@service_store');
        Route::post('/se-paycredit', 'CashierController@se_paycredit');
        Route::post('/se-paybalance', 'CashierController@se_paybalance');
        Route::post('/se-destroy', 'CashierController@se_destroy');
        Route::post('/se-install', 'CashierController@se_install');
    });

    // OWNER GROUP
    Route::group(['middleware'=>'role:owner','prefix'=>'owner'], function (){
        Route::resource('/dashboard', 'LogController');
        Route::resource('/manage-user', 'UserController');
        Route::resource('/manage-product', 'ProductController');
        Route::resource('/manage-sale', 'SaleController');

        Route::get('/findtrans', 'SaleController@findtrans');
        Route::post('/paynow', 'SaleController@paynow');
        Route::post('/paybalance', 'SaleController@paybalance');
        Route::post('/addinstallment', 'SaleController@addinstallment');
        Route::get('/account', 'UserController@account');
        Route::post('/update-account', 'UserController@account_update');
        Route::post('/remove-trans', 'SaleController@removeTrans');

        /* PHOTOCOPY/XEROX TOOL */
        Route::get('/photocopy-xerox/{type?}/{when?}', 'CashierController@photocopy_xerox');
        Route::post('/photocopy-xerox', 'CashierController@photocopy_xerox_store');
        Route::post('/px-paycredit', 'CashierController@px_paycredit');
        Route::post('/px-paybalance', 'CashierController@px_paybalance');
        Route::post('/px-destroy', 'CashierController@px_destroy');
        Route::post('/px-install', 'CashierController@px_install');

        /* AQUABYTE TOOL */
        Route::get('/aquabyte/{type?}/{when?}', 'CashierController@aquabyte');
        Route::post('/aquabyte', 'CashierController@aquabyte_store');
        Route::post('/aq-paycredit', 'CashierController@aq_paycredit');
        Route::post('/aq-paybalance', 'CashierController@aq_paybalance');
        Route::post('/aq-destroy', 'CashierController@aq_destroy');
        Route::post('/aq-install', 'CashierController@aq_install');

        /* E-LOAD TOOL */
        Route::get('/e-load/{type?}/{when?}', 'CashierController@eload');
        Route::post('/e-load', 'CashierController@eload_store');
        Route::post('/el-paycredit', 'CashierController@el_paycredit');
        Route::post('/el-paybalance', 'CashierController@el_paybalance');
        Route::post('/el-destroy', 'CashierController@el_destroy');
        Route::post('/el-install', 'CashierController@el_install');

        /* HANDS-ON TOOL */
        Route::get('/hands-on/{type?}/{when?}', 'CashierController@hands_on');
        Route::post('/hands-on', 'CashierController@hands_on_store');
        Route::post('/ho-paycredit', 'CashierController@ho_paycredit');
        Route::post('/ho-paybalance', 'CashierController@ho_paybalance');
        Route::post('/ho-destroy', 'CashierController@ho_destroy');
        Route::post('/ho-install', 'CashierController@ho_install');

        /* SERVICES TOOL */
        Route::get('/service/{type?}/{when?}', 'CashierController@service');
        Route::post('/service', 'CashierController@service_store');
        Route::post('/se-paycredit', 'CashierController@se_paycredit');
        Route::post('/se-paybalance', 'CashierController@se_paybalance');
        Route::post('/se-destroy', 'CashierController@se_destroy');
        Route::post('/se-install', 'CashierController@se_install');
    });
});

Route::get('/api/product/{barcode?}', 'ProductController@productToJson');
Route::get('/api/creditnames', 'ProductController@creditNamesToJson');
Route::get('/api/itemprice', 'ProductController@inquireItems');
Route::get('/log/{type}', 'LogController@ajaxLog');
Route::post('/convert-time', 'CashierController@convertTime');




