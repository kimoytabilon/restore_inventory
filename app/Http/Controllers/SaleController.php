<?php

namespace App\Http\Controllers;

use Auth;
use App\Sale;
use App\Cash;
use App\Product;
use App\User;
use App\Log;

use App\ToolPx;
use App\ToolAq;
use App\ToolEl;
use App\ToolHo;
use App\ToolSe;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $active_user = Auth::user();

        /*RECAP*/
        $recap = [
            'avail_products'=>Product::where('quantity','>',0)->count(),
            'all_products'=>Product::count(),
            'item_sold'=>Sale::where('created_at','like',$_GET['r'].'%')->sum('quantity'),
            'item_left'=>Product::where('quantity','>',0)->sum('quantity'),
        ];

        $months = ["01"=>"January", "02"=>"February", "03"=>"March", "04"=>"April", "05"=>"May", "06"=>"June", "07"=>"July", "08"=>"August", "09"=>"September", "10"=>"October", "11"=>"November", "12"=>"December"];
        $tools = ['sa','aq','el','ho','px','se'];
        $year = explode('-',$_GET['r'])[0];
        $cashPerMonth = [];
        foreach($months as $key=>$month) {
            foreach($tools as $tool) {
                $cashPerMonth[$tool][$month] = Cash::where('tool',$tool)->where('created_at','like',$year.'-'.$key.'%')->sum('amount');
            }
        }
        //dd($cashPerMonth);

        /* TOOLS */
        $toolSebal = Cash::where('tool','sa')->where('created_at','like',$_GET['r'].'%')->sum('amount');

        $services = [
            'aq' => [
                'desc' => 'Aquabyte',
                'short' => 'aq',
                'url' => url($active_user->role.'/aquabyte'),
                'cash' => Cash::where('tool','aq')->where('created_at','like',$_GET['r'].'%')->sum('amount'),
                'amount' => ToolAq::where('created_at','like',$_GET['r'].'%')->where('remarks','!=', 1)->sum('amount'),
                'credit' => ToolAq::where('created_at','like',$_GET['r'].'%')->where('remarks', 1)->sum('amount'),
                'collection' => ToolAq::where('created_at','like',$_GET['r'].'%')->where('remarks', 1)->sum('collection'),
                'color' => 'green',
                'asci' => '#00a65a',
            ],
            'el' => [
                'desc' => 'E-Load',
                'short' => 'el',
                'url' => url($active_user->role.'/e-load'),
                'cash' => Cash::where('tool','el')->where('created_at','like',$_GET['r'].'%')->sum('amount'),
                'amount' => ToolEl::where('created_at','like',$_GET['r'].'%')->where('remarks','!=', 1)->sum('amount'),
                'credit' => ToolEl::where('created_at','like',$_GET['r'].'%')->where('remarks', 1)->sum('amount'),
                'collection' => ToolEl::where('created_at','like',$_GET['r'].'%')->where('remarks', 1)->sum('collection'),
                'color' => 'yellow',
                'asci' => '#f39c12',
            ],
            'ho' => [
                'desc' => 'Hands-on',
                'short' => 'ho',
                'url' => url($active_user->role.'/hands-on'),
                'cash' => Cash::where('tool','ho')->where('created_at','like',$_GET['r'].'%')->sum('amount'),
                'amount' => ToolHo::where('time_in','like',$_GET['r'].'%')->where('remarks','!=', 1)->sum('amount'),
                'credit' => ToolHo::where('time_in','like',$_GET['r'].'%')->where('remarks', 1)->sum('amount'),
                'collection' => ToolHo::where('time_in','like',$_GET['r'].'%')->where('remarks', 1)->sum('collection'),
                'color' => 'aqua',
                'asci' => '#00c0ef',
            ],
            'px' => [
                'desc' => 'Photocopy',
                'short' => 'px',
                'url' => url($active_user->role.'/photocopy-xerox'),
                'cash' => Cash::where('tool','px')->where('created_at','like',$_GET['r'].'%')->sum('amount'),
                'amount' => ToolPx::where('created_at','like',$_GET['r'].'%')->where('remarks','!=', 1)->sum('amount'),
                'credit' => ToolPx::where('created_at','like',$_GET['r'].'%')->where('remarks', 1)->sum('amount'),
                'collection' => ToolPx::where('created_at','like',$_GET['r'].'%')->where('remarks', 1)->sum('collection'),
                'color' => 'light-blue',
                'asci' => '#3c8dbc',
            ],
            'se' => [
                'desc' => 'Services',
                'short' => 'se',
                'url' => url($active_user->role.'/service'),
                'cash' => Cash::where('tool','se')->where('created_at','like',$_GET['r'].'%')->sum('amount'),
                'amount' => ToolSe::where('created_at','like',$_GET['r'].'%')->where('remarks','!=', 1)->sum('amount'),
                'credit' => ToolSe::where('created_at','like',$_GET['r'].'%')->where('remarks', 1)->sum('amount'),
                'collection' => ToolSe::where('created_at','like',$_GET['r'].'%')->where('remarks', 1)->sum('collection'),
                'color' => 'gray',
                'asci' => '#d2d6de',
            ],
        ];
        /* END OF TOOLS */

        /* PRODUCT */
        $totalSold = 0;

        $productName = '';
        $products = [];
        $products['overalltotal'] = 0;
        $products['overalltotal_today'] = Cash::where('tool','sa')->where('created_at','like',$_GET['r'].'%')->sum('amount');
        $products['overalltotal_cash'] = 0;
        $products['overalltotal_credit'] = 0;
        $products['overalltotal_cash_discount'] = 0;
        $products['overalltotal_credit_discount'] = 0;
        $products['overalltotal_discount'] = 0;
        $products['overalltotal_collection'] = 0;
        $collection = [];

        foreach(Product::orderBy('name')->orderBy('description')->get() as $product) {

            if($productName != $product->name) {
                $products[$product->name]['total_cash_price'] = 0;
                $products[$product->name]['total_credit_price'] = 0;
                $products[$product->name]['total_qty_cash_credit'] = 0;
                $products[$product->name]['total_discount_cash_credit'] = 0;
                $totalSold = 0;
            }

            /*********************/
            $sold_cash_items = Sale::where('created_at','like', $_GET['r'].'%')->where('product_id',$product->id)->where('remarks','!=','1')->get();
            $sold_credit_items = Sale::where('created_at','like', $_GET['r'].'%')->where('product_id',$product->id)->where('remarks','=','1')->get();

            //dd($sold_credit_items);
            $sold_cash_qty=0;
            $sold_cash_price = 0;
            $sold_cash_discount = 0;

            $sold_credit_qty=0;
            $sold_credit_price = 0;
            $sold_credit_discount = 0;

            foreach($sold_cash_items as $sold_cash_item) {
                $sold_cash_qty += $sold_cash_item->quantity;
                $sold_cash_price += $sold_cash_item->quantity * $sold_cash_item->price;
                $sold_cash_discount += $sold_cash_item->discount;
            }
            $no = 1;
            foreach($sold_credit_items as $sold_credit_item) {
                $sold_credit_qty += $sold_credit_item->quantity;
                $sold_credit_price += $sold_credit_item->quantity * $sold_credit_item->price;
                $sold_credit_discount += $sold_credit_item->discount;
                //$sold_credit_collection += $sold_credit_item->partial_payment;

                $collection[$sold_credit_item->transaction_number] = $sold_credit_item->partial_payment;

                //echo $sold_credit_item->transaction_number .' => ' . $sold_credit_item->partial_payment . '<hr>';
                //$no++;
            }

            $total_qty_cash_credit = $sold_cash_qty + $sold_credit_qty;
            /*********************/

            /* TOTAL SOLD IN EACH ITEM ++*/
            $totalSold += $total_qty_cash_credit;

            if($total_qty_cash_credit!=0) {

                if($sold_cash_price!='0.0' || $sold_credit_price!='0.0') {
                    $products['overalltotal'] += ($sold_cash_price + $sold_credit_price);
                    $products['overalltotal_cash'] += $sold_cash_price;
                    $products['overalltotal_credit'] += $sold_credit_price;
                    $products['overalltotal_cash_discount'] += $sold_cash_discount;
                    $products['overalltotal_credit_discount'] += $sold_credit_discount;
                    $products['overalltotal_discount'] += ($sold_credit_discount + $sold_cash_discount);
                    //$products['overalltotal_collection'] = $sold_credit_collection;

                    $products[$product->name]['total_cash_price'] += $sold_cash_price;
                    $products[$product->name]['total_credit_price'] += $sold_credit_price;
                    $products[$product->name]['total_qty_cash_credit'] += $total_qty_cash_credit;
                    $products[$product->name]['total_discount_cash_credit'] += ($sold_credit_discount + $sold_cash_discount);
                }
            }

            $productName = $product->name;

            $products[$product->name]['total_sold'] = $totalSold;

            $products[$product->name][] = [
                'id'=>$product->id,
                'code'=>$product->code,
                'description'=>$product->description,
                'quantity'=>$product->quantity,
                'unit'=>$product->unit,
                'original_price'=>$product->original_price,
                'cash_price'=>$product->cash_price,
                'credit_price'=>$product->credit_price,
                'sold_qty_cash'=>$sold_cash_qty,
                'sold_total_cash'=>$sold_cash_price,
                'sold_qty_credit'=>$sold_credit_qty,
                'sold_total_credit'=>$sold_credit_price,
                'sold_cash_discount'=>$sold_cash_discount,
                'sold_credit_discount'=>$sold_credit_discount,
                'sold_total_discount'=>($sold_credit_discount + $sold_cash_discount),
                'sold_qty_total'=>$total_qty_cash_credit,
            ];
        }
        foreach($collection as $collect) { $products['overalltotal_collection'] += $collect; }
        /* END OF PRODUCT */

        /* CASHIER */
        $cashier = [];

        if($active_user->role == 'cashier' ) { $active_cashier = User::where('id',$active_user->id)->get(); }
        else { $active_cashier = User::where('role','cashier')->get(); }

        foreach($active_cashier as $cashiers) {
            $cashier[$cashiers->id] = [
                'id' => $cashiers->id, 'name' => $cashiers->name, 'total_sold' => Cash::where('user_id',$cashiers->id)->where('created_at','like', $_GET['r'].'%')->sum('amount'),
            ];
        }
        /* END OF CASHIER */

        /* CREDITS **/
        $credits = [];
        $credit_info='';
        $transaction_number='';

        foreach(Sale::where('created_at','like',$_GET['r'].'%')->where('remarks',1)->get() as $credit) {
            if($credit_info==''||$credit_info!=$credit->credit_info) {
                $credits[$credit->credit_info]['total_amount'] = isset($credits[$credit->credit_info]['total_amount'])?$credits[$credit->credit_info]['total_amount']:0;
                $credits[$credit->credit_info]['total_partial'] = isset($credits[$credit->credit_info]['total_partial'])?$credits[$credit->credit_info]['total_partial']:0;
                $credits[$credit->credit_info]['total_discount'] = isset($credits[$credit->credit_info]['total_discount'])?$credits[$credit->credit_info]['total_discount']:0;
            }
            if($transaction_number==''||$credit->transaction_number!=$transaction_number) {
                $credits[$credit->credit_info][$credit->transaction_number]['amount'] = 0;
                $credits[$credit->credit_info][$credit->transaction_number]['discount'] = 0;
                $credits[$credit->credit_info]['total_partial'] += $credit->partial_payment;
            }

            $credit_info=$credit->credit_info;
            $transaction_number=$credit->transaction_number;

            $credits[$credit->credit_info][$credit->transaction_number]['discount'] += $credit->discount;
            $credits[$credit->credit_info][$credit->transaction_number]['amount'] += ($credit->price * $credit->quantity) - $credit->discount;
            $credits[$credit->credit_info][$credit->transaction_number]['partial'] = $credit->partial_payment;
            $credits[$credit->credit_info][$credit->transaction_number]['created_at'] = $credit->created_at->format('h:i a');

            //$credits[$credit->credit_info]['total_amount'] += ($credit->price * $credit->quantity);
            $credits[$credit->credit_info]['total_amount'] += ($credit->price * $credit->quantity) - $credit->discount;
            $credits[$credit->credit_info]['total_discount'] += $credit->discount;

            $credits[$credit->credit_info][$credit->transaction_number][] = [
                'quantity'=>$credit->quantity, 'price'=>$credit->price, 'discount'=>$credit->discount,
            ];
        }

        $tools = [
            'aq' => ToolAq::where('created_at','like',$_GET['r'].'%')->where('remarks',1)->get(),
            'el' => ToolEl::where('created_at','like',$_GET['r'].'%')->where('remarks',1)->get(),
            'ho' => ToolHo::where('time_in','like',$_GET['r'].'%')->where('remarks',1)->get(),
            'px' => ToolPx::where('created_at','like',$_GET['r'].'%')->where('remarks',1)->get(),
            'se' => ToolSe::where('created_at','like',$_GET['r'].'%')->where('remarks',1)->get(),
        ];

        foreach($tools as $key=>$tool) {
            foreach($tool as $credit) {
                if($credit_info==''||$credit_info!=$credit->customer) {
                    $credits[$credit->customer]['total_amount'] = isset($credits[$credit->customer]['total_amount'])?$credits[$credit->customer]['total_amount']:0;
                    $credits[$credit->customer]['total_partial'] = isset($credits[$credit->customer]['total_partial'])?$credits[$credit->customer]['total_partial']:0;
                    $credits[$credit->customer]['total_discount'] = isset($credits[$credit->customer]['total_discount'])?$credits[$credit->customer]['total_discount']:0;
                }
                if($credit->transaction_number!=$transaction_number) {
                    $credits[$credit->customer][$credit->transaction_number]['amount'] = isset($credits[$credit->customer][$credit->transaction_number]['amount'])?$credits[$credit->customer][$credit->transaction_number]['amount']:0;
                    $credits[$credit->customer][$credit->transaction_number]['discount'] = isset($credits[$credit->customer][$credit->transaction_number]['discount'])?$credits[$credit->customer][$credit->transaction_number]['discount']:0;
                    $credits[$credit->customer][$credit->transaction_number]['partial'] = isset($credits[$credit->customer][$credit->transaction_number]['partial'])?$credits[$credit->customer][$credit->transaction_number]['partial']:0;
                }
                $credit_info=$credit->customer;
                $transaction_number=$credit->transaction_number;

                $credits[$credit->customer][$credit->transaction_number]['discount'] += $credit->discount;
                $credits[$credit->customer][$credit->transaction_number]['amount'] += $credit->amount;
                $credits[$credit->customer][$credit->transaction_number]['partial'] += $credit->collection;

                $credits[$credit->customer]['total_partial'] += $credit->collection;
                $credits[$credit->customer]['total_discount'] += $credit->discount;
                $credits[$credit->customer]['total_amount'] += $credit->amount;

                $credits[$credit->customer][$credit->transaction_number]['created_at'] = $credit->created_at->format('h:i a');

                $credits[$credit->customer][$credit->transaction_number][] = [
                    'quantity'=>$credit->quantity, 'price'=>$credit->price, 'discount'=>$credit->discount,
                ];
            }
        }


        //dd($credits);
        /* END OF CREDITS */

        /* TRANSACTIONS */
        $transactions = [];
        $trans_first = '';
        foreach(Sale::where('created_at','like',$_GET['r'].'%')->orderBy('created_at','DESC')->get() as $transaction) {

            if($trans_first != $transaction->transaction_number) {
                $trans_total = 0;
                $trans_discount = 0;
                $trans_total += ($transaction->quantity * $transaction->price) - $transaction->discount;
                //$trans_total += $transaction->amount;
            }
            if($trans_first == $transaction->transaction_number) {
                $trans_total += ($transaction->quantity * $transaction->price) - $transaction->discount;
                //$trans_total += $transaction->amount;
            }
            $trans_discount += $transaction->discount;
            $trans_first = $transaction->transaction_number;

            $transactions[$transaction->transaction_number]['total_amount'] = $trans_total;
            $transactions[$transaction->transaction_number]['total_discount'] = $trans_discount;

            $transactions[$transaction->transaction_number][] = [
                'transaction_number'=>$transaction->transaction_number, 'price'=>$transaction->price, 'quantity'=>$transaction->quantity, 'remarks'=>$transaction->remarks, 'credit_info'=>$transaction->credit_info,
            ];
        }

        $tools = [
            'Aquabyte' => ToolAq::where('created_at','like',$_GET['r'].'%')->orderBy('created_at','DESC')->get(),
            'E-Load' => ToolEl::where('created_at','like',$_GET['r'].'%')->orderBy('created_at','DESC')->get(),
            'Hands-on' => ToolHo::where('time_in','like',$_GET['r'].'%')->orderBy('time_in','DESC')->get(),
            'Photocopy' => ToolPx::where('created_at','like',$_GET['r'].'%')->orderBy('created_at','DESC')->get(),
            'Service' => ToolSe::where('created_at','like',$_GET['r'].'%')->orderBy('created_at','DESC')->get(),
        ];

        foreach($tools as $tool) {
            if($tool->count()) {
                foreach($tool as $transaction) {
                    if($trans_first != $transaction->transaction_number) {
                        $trans_total = 0;
                        $trans_discount = 0;
                        $trans_total += $transaction->amount;
                        if(isset($transactions[$transaction->transaction_number]['total_amount'])){
                            $trans_total += $transactions[$transaction->transaction_number]['total_amount'];
                        }
                    }
                    if($trans_first == $transaction->transaction_number) {
                        $trans_total += $transaction->amount;
                    }

                    $trans_discount += $transaction->discount;
                    $trans_first = $transaction->transaction_number;

                    $transactions[$transaction->transaction_number]['total_amount'] = $trans_total;
                    $transactions[$transaction->transaction_number]['total_discount'] = $trans_discount;

                    $transactions[$transaction->transaction_number][] = [
                        'transaction_number'=>$transaction->transaction_number,
                        'price'=>$transaction->price,
                        'quantity'=>$transaction->quantity,
                        'remarks'=>$transaction->remarks,
                        'credit_info'=>$transaction->customer,
                    ];
                }
            }

        }


        //dd($transactions);
        /* END OF TRANSACTIONS */

        /* TOOL AQUABYTE */
        $toolAqs = [];
        $toolAqs['overall_cash'] = 0;
        $toolAqs['overall_credit'] = 0;
        $toolAqs['overall_collection'] = 0;
        $toolAqs['overall_gals'] = 0;
        $customer = '';
        foreach(ToolAq::where('created_at','like',$_GET['r'].'%')->get() as $aq) {
            if($aq->remarks == 1) {
                $toolAqs['overall_credit'] += $aq->amount;
                $toolAqs['overall_collection'] += $aq->collection;
            }
            else {
                $toolAqs['overall_cash'] += $aq->amount;
            }
            $toolAqs[] = [
                'customer' => $aq->customer, 'gal' => $aq->quantity, 'remarks' => $aq->remarks, 'amount' => $aq->amount, 'collection' => $aq->collection,
            ];
            $toolAqs['overall_gals'] += $aq->quantity;
        }

        /* TOOL ELOAD */
        $toolEls = [];
        $toolEls['overall_cash'] = 0;
        $toolEls['overall_credit'] = 0;
        $toolEls['overall_collection'] = 0;
        $toolEls['overall_load'] = 0;
        foreach(ToolEl::where('created_at','like',$_GET['r'].'%')->get() as $el) {
            if($el->remarks == 1) {
                $toolEls['overall_credit'] += $el->amount;
                $toolEls['overall_collection'] += $el->collection;
            }
            else { $toolEls['overall_cash'] += $el->amount; }
            $toolEls[] = [
                'load' => $el->quantity, 'network' => $el->network, 'customer' => $el->customer, 'number' => $el->number, 'remarks' => $el->remarks, 'amount' => $el->amount, 'collection' => $el->collection,
            ];
            $toolEls['overall_load'] += $el->quantity;
        }

        /* TOOL SERVICE */
        $toolSes = [];
        $toolSes['overall_cash'] = 0;
        $toolSes['overall_credit'] = 0;
        $toolSes['overall_collection'] = 0;
        foreach(ToolSe::where('created_at','like',$_GET['r'].'%')->get() as $se) {
            if($customer==''||$customer!=$se->desc) {
                $toolSes[$se->desc]['cash'] = 0;
                $toolSes[$se->desc]['credit'] = 0;
                $toolSes[$se->desc]['collection'] = 0;
            }
            $customer=$se->desc;
            if($se->remarks == 1) {
                $toolSes[$se->desc]['credit'] += $se->amount;
                $toolSes[$se->desc]['collection'] += $se->collection;
                $toolSes['overall_credit'] += $se->amount;
                $toolSes['overall_collection'] += $se->collection;
            }
            else {
                $toolSes[$se->desc]['cash'] += $se->amount;
                $toolSes['overall_cash'] += $se->amount;
            }
            $toolSes[$se->desc]['customer'] = $se->customer;
        }

        /* TOOL PHOTOCOPY/XEROX */
        $toolPxs = [];
        $toolPxs['overall_pcs'] = 0;
        $toolPxs['overall_cash'] = 0;
        $toolPxs['overall_credit'] = 0;
        $toolPxs['overall_collection'] = 0;
        foreach(ToolPx::where('created_at','like',$_GET['r'].'%')->get() as $px) {
            $toolPxs['overall_pcs'] += $px->pcs;
            $toolPxs['overall_collection'] += $px->collection;
            if($px->remarks != 1) {
                $toolPxs['overall_cash'] += $px->amount;
            }
            else { $toolPxs['overall_credit'] += $px->amount; }
            $toolPxs[] = [
                'pcs' => $px->pcs, 'customer' => $px->customer, 'remarks' => $px->remarks, 'amount' => $px->amount, 'collection' => $px->collection,
            ];
        }

        /* TOOL HANDS-ON */
        $toolHos = [];
        $toolHos['overall_cash'] = 0;
        $toolHos['overall_credit'] = 0;
        $toolHos['overall_collection'] = 0;
        foreach(ToolHo::where('created_at','like',$_GET['r'].'%')->orderBy('time_in', 'desc')->get() as $ho) {
            if($ho->remarks == 1) {
                $toolHos['overall_credit'] += $ho->amount;
                $toolHos['overall_collection'] += $ho->collection;
            }
            else { $toolHos['overall_cash'] += $ho->amount; }
            $toolHos[] = [
                'time_in' => $ho->time_in, 'time_out' => $ho->time_out, 'customer' => $ho->customer, 'remarks' => $ho->remarks, 'amount' => $ho->amount, 'collection' => $ho->collection,
            ];
        }

        return view('partials.manage-sale')
            ->with('products', $products)
            ->with('cashier', $cashier)
            ->with('credits', $credits)
            ->with('transactions', $transactions)
            ->with('role', Auth::user()->role)
            ->with('services', $services)
            ->with('aqs', $toolAqs)
            ->with('pxs', $toolPxs)
            ->with('els', $toolEls)
            ->with('ses', $toolSes)
            ->with('hos', $toolHos)
            ->with('sebal', $toolSebal)
            ->with('cashPerMonth', $cashPerMonth)
            ->with('recap', $recap);
    }

    public function findtrans()
    {
        $sale = [];
        $sale['date'] = '';
        $sale['remarks'] = '';
        $sale['credit_info'] = '';
        $sale['date'] = '';
        $sale['partial_payment'] = '';

        foreach(Sale::where('transaction_number',$_GET['q'])->get() as $sales) {

            $item = Product::find($sales->product_id,['description','unit']);

            $sale['remarks'] = $sales->remarks;
            $sale['credit_info'] = $sales->credit_info;
            $sale['date'] = Carbon::parse($sales->created_at)->format('M. d - ') . Carbon::parse($sales->created_at)->diffForHumans();
            $sale['partial_payment'] = $sales->partial_payment;

            $sale['sa_'.$sales->id]['quantity'] = $sales->quantity;
            $sale['sa_'.$sales->id]['price'] = $sales->price;
            $sale['sa_'.$sales->id]['discount'] = $sales->discount;
            $sale['sa_'.$sales->id]['amount'] = ($sales->quantity * $sales->price) - $sales->discount;
            $sale['sa_'.$sales->id]['unit'] = $item->unit;
            $sale['sa_'.$sales->id]['description'] = $item->description;
        }

        $tools = [
            'Aquabyte,gal' => ToolAq::where('transaction_number',$_GET['q'])->get(),
            'E-Load,load' => ToolEl::where('transaction_number',$_GET['q'])->get(),
            'Hands-on,hour' => ToolHo::where('transaction_number',$_GET['q'])->get(),
            'Photocopy,pc' => ToolPx::where('transaction_number',$_GET['q'])->get(),
            'Service,service' => ToolSe::where('transaction_number',$_GET['q'])->get(),
        ];

        foreach($tools as $key=>$tool) {
            foreach($tool as $sales) {
                $sale['date'] = Carbon::parse($sales->created_at)->format('M. d - ') . Carbon::parse($sales->created_at)->diffForHumans();
                $sale['remarks'] = $sales->remarks;
                $sale['credit_info'] = $sales->customer;
                $sale['partial_payment'] += $sales->collection;
                $sale[explode(',',$key)[1].'_'.$sales->id]['quantity'] = $sales->quantity;
                $sale[explode(',',$key)[1].'_'.$sales->id]['price'] = $sales->price;
                $sale[explode(',',$key)[1].'_'.$sales->id]['discount'] = $sales->discount;
                $sale[explode(',',$key)[1].'_'.$sales->id]['amount'] = $sales->amount;
                $sale[explode(',',$key)[1].'_'.$sales->id]['unit'] = explode(',',$key)[1];
                if($key=='Service,service') { $sale[explode(',',$key)[1].'_'.$sales->id]['description'] = $sales->desc; }
                else{ $sale[explode(',',$key)[1].'_'.$sales->id]['description'] = explode(',',$key)[0]; }
            }
        }
        //Log::create(['user_id'=>Auth::user()->id, 'type'=>11, 'title'=>'Searching transaction #'.$_GET['q']]);

        return view('partials.find-trans-number', ['role'=>Auth::user()->role, 'sold' => $sale, 'trans'=>$_GET['q'], 'total_amount'=>0]);
    }

    public function paynow(Request $request)
    {
        $pendingitems = [
            'sa' => Sale::where('transaction_number',$request->transaction)->where('remarks',1)->get(),
            'aq' => ToolAq::where('transaction_number',$request->transaction)->where('remarks',1)->get(),
            'el' => ToolEl::where('transaction_number',$request->transaction)->where('remarks',1)->get(),
            'ho' => ToolHo::where('transaction_number',$request->transaction)->where('remarks',1)->get(),
            'px' => ToolPx::where('transaction_number',$request->transaction)->where('remarks',1)->get(),
            'se' => ToolSe::where('transaction_number',$request->transaction)->where('remarks',1)->get(),
        ];
        $amountdiscount = $request->actual - $request->amount;
        foreach($pendingitems as $key=>$items) {
            if($items->count()) {
                if($key=='sa') {
                    foreach($items as $item) {
                        $balance = $item->total_amount - $item->partial_payment;
                        $item->remarks = 2;
                        $item->partial_payment = 0;
                        $item->update();
                    }
                    Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'sa', 'amount'=>$balance - $amountdiscount, 'transaction_number'=>$request->transaction]);
                    $amountdiscount = 0;
                }
                else {
                    foreach($items as $item) {
                        $balance = $item->amount - $item->collection;
                        Cash::create(['user_id'=>Auth::user()->id, 'tool'=>$key, 'amount'=>$balance - $amountdiscount, 'transaction_number'=>$request->transaction]);
                        $amountdiscount = 0;
                        $item->remarks = 2;
                        $item->collection = 0;
                        $item->update();
                    }
                }
            }
        }
        Log::create(['user_id'=>Auth::user()->id, 'type'=>12, 'title'=>'Payment of transaction #'.$request->transaction]);

        return 1;
    }

    public function paybalance(Request $request)
    {
        $pendingbalance = [
            'sa' => Sale::where('credit_info',$request->name)->where('remarks',1)->get(),
            'aq' => ToolAq::where('customer',$request->name)->where('remarks',1)->get(),
            'el' => ToolEl::where('customer',$request->name)->where('remarks',1)->get(),
            'ho' => ToolHo::where('customer',$request->name)->where('remarks',1)->get(),
            'px' => ToolPx::where('customer',$request->name)->where('remarks',1)->get(),
            'se' => ToolSe::where('customer',$request->name)->where('remarks',1)->get(),
        ];
        $balancediscount = $request->actual - $request->balance;
        foreach($pendingbalance as $key=>$items) {
            if($items->count()) {
                if($key=='sa') {
                    $sa_transaction = '';
                    $sa_balance = 0;
                    foreach($items as $item) {
                        if($sa_transaction==''||$sa_transaction!=$item->transaction_number) {
                            $sa_balance = $item->total_amount - $item->partial_payment;
							if($sa_balance>0) {
								if($sa_balance>=$balancediscount) {
									Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'sa', 'amount'=>$sa_balance - $balancediscount, 'transaction_number'=>$item->transaction_number]);
									$balancediscount = 0;
								}
								else {
									Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'sa', 'amount'=>$sa_balance, 'transaction_number'=>$item->transaction_number]);
								}
							}
                        }
                        $sa_transaction = $item->transaction_number;
                        $item->remarks = 2;
                        $item->partial_payment = 0;
                        $item->update();
                    }
                }
                else {
                    foreach($items as $item) {
                        $balance = $item->amount - $item->collection;
						if($balance>0) {
								if($balance>=$balancediscount) {
									Cash::create(['user_id'=>Auth::user()->id, 'tool'=>$key, 'amount'=>$balance - $balancediscount, 'transaction_number'=>$item->transaction_number]);
									$balancediscount = 0;
								}	
								else {
									Cash::create(['user_id'=>Auth::user()->id, 'tool'=>$key, 'amount'=>$balance, 'transaction_number'=>$item->transaction_number]);
								}
						}		
                        
                        $item->remarks = 2;
                        $item->collection = 0;
                        $item->update();
                    }
                }
            }
        }
        //Sale::where('credit_info',$request->name)->update(['remarks'=>2]);
        //Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'sa', 'amount'=>$request->balance]);
        Log::create(['user_id'=>Auth::user()->id, 'type'=>13, 'title'=>'Balance has been paid for '.$request->name.' credit account']);

        return 1;
    }

    public function addinstallment(Request $request)
    {
        $installItems = [
            'sa' => Sale::where('transaction_number',$request->transaction)->where('remarks',1)->get(),
            'aq' => ToolAq::where('transaction_number',$request->transaction)->where('remarks',1)->get(),
            'el' => ToolEl::where('transaction_number',$request->transaction)->where('remarks',1)->get(),
            'ho' => ToolHo::where('transaction_number',$request->transaction)->where('remarks',1)->get(),
            'px' => ToolPx::where('transaction_number',$request->transaction)->where('remarks',1)->get(),
            'se' => ToolSe::where('transaction_number',$request->transaction)->where('remarks',1)->get(),
        ];
        $cash = $request->cash;
        if($cash!=0) {
            foreach($installItems as $key=>$items) {
                if($items->count()) {
                    if($key=='sa') {
                        $zerobalance = 0;
                        foreach($items as $item) {
                            $balance = $item->total_amount - $item->partial_payment;
                            if($balance!=0) {
                                if($cash >= $balance) {
                                    //$item->partial_payment = $item->partial_payment+($balance - 1);
                                    $item->partial_payment = $item->partial_payment+$balance;
                                    $sale_balance = $balance;
                                }
                                else {
                                    $item->partial_payment = $item->partial_payment+$cash;
                                    $sale_balance = $cash;
                                }
                                $item->update();
                            }
                            else {
                                $zerobalance = 1;
                            }
                        }
                        if(!$zerobalance) {
                            Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'sa', 'amount'=>$sale_balance, 'transaction_number'=>$request->transaction]);
                            $cash -= $sale_balance;
                        }

                    }
                    else {
                        foreach($items as $item) {
                            $balance = $item->amount - $item->collection;
                            if($balance!=0) {
                                if($cash >= $balance) {
                                    //$item->collection = $item->collection+($balance - 1);
                                    $item->collection = $item->collection+$balance;
                                    $sale_balance = $balance;
                                }
                                else {
                                    $item->collection = $item->collection+$cash;
                                    $sale_balance = $cash;
                                }
                                $item->update();
                                Cash::create(['user_id'=>Auth::user()->id, 'tool'=>$key, 'amount'=>$sale_balance, 'transaction_number'=>$request->transaction]);
                                $cash -= $sale_balance;
                            }
                        }
                    }
                }

            }
        }
        Log::create(['user_id'=>Auth::user()->id, 'type'=>14, 'title'=>'Install '.$request->cash.' pesos to credit with transaction #'.$request->transaction]);

        return 1;
    }

    public function removeTrans(Request $request)
    {
        $alltrans = [
            'sa' => Sale::where('transaction_number', $request->trans)->where('created_at', 'like', $request->date.'%')->get(),
            'aq' => ToolAq::where('transaction_number', $request->trans)->where('created_at', 'like', $request->date.'%')->get(),
            'el' => ToolEl::where('transaction_number', $request->trans)->where('created_at', 'like', $request->date.'%')->get(),
            'ho' => ToolHo::where('transaction_number', $request->trans)->where('time_in', 'like', $request->date.'%')->get(),
            'px' => ToolPx::where('transaction_number', $request->trans)->where('created_at', 'like', $request->date.'%')->get(),
            'se' => ToolSe::where('transaction_number', $request->trans)->where('created_at', 'like', $request->date.'%')->get(),
        ];
        foreach($alltrans as $trans) {
            if($trans->count()) {
                foreach($trans as $tran){
                    $tran->delete();
                }
            }
        }
        Cash::where('transaction_number', $request->trans)->delete();

        return 1;
    }
}
