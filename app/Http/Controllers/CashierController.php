<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use Auth;
use App\Product;
use App\Sale;
use App\Cash;
use App\ToolPx;
use App\ToolAq;
use App\ToolEl;
use App\ToolHo;
use App\ToolSe;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CashierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $products = [];
        $names = [];
        foreach(Product::orderBy('name')->orderBy('description')->get() as $product)
        {
            $products[$product->name][] = [
                'id'=>$product->id,
                'code'=>$product->code,
                'description'=>$product->description,
                'quantity'=>$product->quantity,
                'original_price'=>$product->original_price,
                'cash_price'=>$product->cash_price,
                'credit_price'=>$product->credit_price,
            ];
        }

        foreach($products as $productName => $productInfo)
        {
            $names[] = $productName;
        }
        return view('partials.cashier.sale',['productNames'=>$names, 'products'=>$products, 'cashier'=>Auth::user()->name]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        /*PRODUCTS*/
        $item_total_amount = 0;
        $item_partial_payment = 0;
        $remaining_partial = $request->partial_payment;

        if($request->items) {
            foreach($request->discount as $dval) {
                $discount[$dval['name']] = $dval['value'];
            }

            /*TOTAL AMOUNT FOR SALE*/
            foreach($request->items as $val) {
                $en = explode(',', $val['name']);
                $item_total_amount += (($en[1]*$val['value']) - $discount[$en[0]]);
            }

            /*CHECK IF CREDIT OR CASH*/
            if($request->remarks==0) {
                Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'sa','amount'=>round($item_total_amount, 2), 'transaction_number'=>$request->transaction_number]);
            }
            if($request->remarks==1&&($request->partial_payment!=0 || $request->partial_payment!='')) {
                if($request->partial_payment>=$item_total_amount) {
                    //$item_partial_payment = $item_total_amount - 1;
                    $item_partial_payment = $item_total_amount;
                }
                else {
                    $item_partial_payment = $remaining_partial;
                }
                $remaining_partial -= $item_partial_payment;
                Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'sa','amount'=>$item_partial_payment, 'transaction_number'=>$request->transaction_number]);
            }

            /*SET REMAINING PARTIAL DEDUCTED FROM SALE*/
            //$remaining_partial = $request->partial_payment - $item_partial_payment;

            foreach($request->items as $val) {
                $en                       = explode(',',$val['name']);
                $sale                     = new Sale();
                $sale->transaction_number = $request->transaction_number;
                $sale->remarks            = $request->remarks;
                $sale->credit_info        = $request->credit_info;
                $sale->partial_payment    = $item_partial_payment;
                $sale->discount           = $discount[$en[0]];
                $sale->price              = $en[1];
                $sale->total_amount       = round($item_total_amount, 2);
                $sale->cash_render        = $request->cash_render;
                $sale->total_change       = round($request->total_change, 2);
                $sale->product_id         = $en[0];
                $sale->quantity           = $val['value'];

                Auth::user()->sales()->save($sale);

                /*UPDATE QUANTITY LEFT*/
                $product           = Product::find($en[0]);
                $qty               = $product->quantity;
                $product->quantity = $qty - $val['value'];
                $product->update();
            }
        }

        if($request->other_items){
            $partial_payment = 0;
            foreach($request->other_items as $services) {
                $service = explode(',',$services['value']);
                switch($services['name']) {
                    case 'aq' :
                        $gal = $service[0];
                        $price = $service[1];
                        $discount = $service[2];
                        $amount = $service[3];
                        if($request->remarks==0) {
                            Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'aq','amount'=>$amount, 'transaction_number'=>$request->transaction_number]);
                        }
                        if($request->remarks == 1 && ($request->partial_payment != 0 || $request->partial_payment != '') && $remaining_partial!=0) {
                            if($remaining_partial >= $amount) {
                                //$partial_payment = $amount - 1;
                                $partial_payment = $amount;
                                $remaining_partial = $remaining_partial - $partial_payment;
                            }
                            else {
                                $partial_payment = $remaining_partial;
                                $remaining_partial = 0;
                            }
                            Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'aq','amount'=>$partial_payment, 'transaction_number'=>$request->transaction_number]);
                        } else { $partial_payment = 0; }
                        ToolAq::create([
                            'user_id'            => Auth::user()->id,
                            'customer'           => $request->credit_info,
                            'transaction_number' => $request->transaction_number,
                            'remarks'            => $request->remarks,
                            'quantity'           => $gal,
                            'price'              => $price,
                            'amount'             => $amount,
                            'discount'           => $discount,
                            'collection'         => $partial_payment,
                        ]);
                        break;

                    case 'el' :
                        $load = $service[0];
                        $price = $service[1];
                        $discount = $service[2];
                        $amount = $service[3];
                        $network = $service[4];
                        $number = $service[5];
                        if($request->remarks==0) {
                            Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'el','amount'=>$amount, 'transaction_number'=>$request->transaction_number]);
                        }
                        if($request->remarks == 1 && ($request->partial_payment != 0 || $request->partial_payment != '') && $remaining_partial!=0) {
                            if($remaining_partial >= $amount) {
                                //$partial_payment = $amount - 1;
                                $partial_payment = $amount;
                                $remaining_partial = $remaining_partial - $partial_payment;
                            }
                            else {
                                $partial_payment = $remaining_partial;
                                $remaining_partial = 0;
                            }
                            Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'el','amount'=>$partial_payment, 'transaction_number'=>$request->transaction_number]);
                        } else { $partial_payment = 0; }
                        ToolEl::create([
                            'user_id'            => Auth::user()->id,
                            'customer'           => $request->credit_info,
                            'transaction_number' => $request->transaction_number,
                            'remarks'            => $request->remarks,
                            'network'            => $network,
                            'number'             => $number,
                            'quantity'           => $load,
                            'amount'             => $amount,
                            'price'              => $price,
                            'discount'           => $discount,
                            'collection'         => $partial_payment,
                        ]);
                        break;

                    case 'ho' :
                        $price = $service[0];
                        $discount = $service[1];
                        $amount = $service[2];
                        $time_in = $service[3];
                        $time_out = $service[4];
                        $time_used = $service[5];
                        if($request->remarks==0) {
                            Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'ho','amount'=>$amount, 'transaction_number'=>$request->transaction_number]);
                        }
                        if($request->remarks == 1 && ($request->partial_payment != 0 || $request->partial_payment != '') && $remaining_partial!=0) {
                            if($remaining_partial >= $amount) {
                                //$partial_payment = $amount - 1;
                                $partial_payment = $amount;
                                $remaining_partial = $remaining_partial - $partial_payment;
                            }
                            else {
                                $partial_payment = $remaining_partial;
                                $remaining_partial = 0;
                            }
                            Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'ho','amount'=>$partial_payment, 'transaction_number'=>$request->transaction_number]);
                        } else { $partial_payment = 0; }
                        ToolHo::create([
                            'user_id'            => Auth::user()->id,
                            'customer'           => $request->credit_info,
                            'transaction_number' => $request->transaction_number,
                            'remarks'            => $request->remarks,
                            'quantity'           => $time_used,
                            'amount'             => $amount,
                            'price'              => $price,
                            'discount'           => $discount,
                            'collection'         => $partial_payment,
                            'time_in'            => $time_in,
                            'time_out'           => $time_out,
                        ]);
                        break;

                    case 'px' :
                        $pc = $service[0];
                        $price = $service[1];
                        $discount = $service[2];
                        $amount = $service[3];
                        if($request->remarks==0) {
                            Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'px','amount'=>$amount, 'transaction_number'=>$request->transaction_number]);
                        }
                        if($request->remarks == 1 && ($request->partial_payment != 0 || $request->partial_payment != '') && $remaining_partial!=0) {
                            if($remaining_partial >= $amount) {
                                //$partial_payment = $amount - 1;
                                $partial_payment = $amount;
                                $remaining_partial = $remaining_partial - $partial_payment;
                            }
                            else {
                                $partial_payment = $remaining_partial;
                                $remaining_partial = 0;
                            }
                            Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'px','amount'=>$partial_payment, 'transaction_number'=>$request->transaction_number]);
                        } else { $partial_payment = 0; }
                        ToolPx::create([
                            'user_id'            => Auth::user()->id,
                            'customer'           => $request->credit_info,
                            'transaction_number' => $request->transaction_number,
                            'remarks'            => $request->remarks,
                            'quantity'           => $pc,
                            'price'              => $price,
                            'amount'             => $amount,
                            'discount'           => $discount,
                            'collection'         => $partial_payment,
                        ]);
                        break;

                    case 'se' :
                        $qty = $service[0];
                        $price = $service[1];
                        $discount = $service[2];
                        $amount = $service[3];
                        $desc = $service[4];
                        if($request->remarks==0) {
                            Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'se','amount'=>$amount, 'transaction_number'=>$request->transaction_number]);
                        }
                        if($request->remarks == 1 && ($request->partial_payment != 0 || $request->partial_payment != '') && $remaining_partial!=0) {
                            if($remaining_partial >= $amount) {
                                //$partial_payment = $amount - 1;
                                $partial_payment = $amount;
                                $remaining_partial = $remaining_partial - $partial_payment;
                            }
                            else {
                                $partial_payment = $remaining_partial;
                                $remaining_partial = 0;
                            }
                            Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'se','amount'=>$partial_payment, 'transaction_number'=>$request->transaction_number]);
                        } else { $partial_payment = 0; }
                        ToolSe::create([
                            'user_id'            => Auth::user()->id,
                            'customer'           => $request->credit_info,
                            'transaction_number' => $request->transaction_number,
                            'remarks'            => $request->remarks,
                            'desc'               => $desc,
                            'quantity'           => $qty,
                            'price'              => $price,
                            'amount'             => $amount,
                            'discount'           => $discount,
                            'collection'         => $partial_payment,
                        ]);
                        break;
                }
            }
        }
        return 1;

    }

    /* PHOTOCOPY/XEROX TOOL */
    public function photocopy_xerox($type = 'Daily', $when = '2016')
    {
        $pxs = [];
        $pxs['overall_pcs'] = 0;
        $pxs['overall_amount'] = 0;
        $pxs['overall_collection'] = 0;
        $customer = '';
        foreach(ToolPx::where('created_at','like',$when.'%')->orderBy('customer')->orderBy('remarks', 'desc')->orderBy('created_at', 'desc')->get() as $px) {
            if($customer=='' || $customer!=$px->customer) {
                $pxs[$px->customer]['total_pcs'] = 0;
                $pxs[$px->customer]['total_amount'] = 0;
                $pxs[$px->customer]['total_collection'] = 0;
            }
            $customer = $px->customer;

            if($px->remarks == 1) {
                $pxs[$px->customer]['total_pcs'] += $px->pcs;
                $pxs[$px->customer]['total_amount'] += $px->amount;
                $pxs[$px->customer]['total_collection'] += $px->collection;
            }
            else {
                $pxs['overall_pcs'] += $px->pcs;
                $pxs['overall_amount'] += $px->amount;
            }
            $pxs['overall_collection'] += $px->collection;

            $pxs[$px->customer][] = [
                'id' => $px->id,
                'transaction' => $px->transaction_number,
                'pcs' => $px->quantity,
                'amount' => $px->amount,
                'remarks' => $px->remarks,
                'collection' => $px->collection,
                'created_at' => $px->created_at,
            ];
        }
        return view('partials.cashier.photocopy-xerox', ['role'=>Auth::user()->role, 'type'=>$type, 'when'=>$when, 'pxs'=>$pxs]);
    }

    public function photocopy_xerox_store(Request $request)
    {
        $px = new ToolPx($request->all());
        Auth::user()->toolpx()->save($px);
        if($request->remarks == 0) {
            Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'px', 'amount'=>$request->amount]); }
        if($request->remarks == 1&&($request->collection!=0 || $request->collection!='')) {
            Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'px', 'amount'=>$request->collection]); }
        return 1;
    }

    public function px_paycredit(Request $request)
    {
        $px = ToolPx::find($request->id);
        $px->collection = 0;
        $px->remarks = 2;
        $px->update();

        Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'px', 'amount'=>$request->amount]);
        return 1;
    }

    public function px_paybalance(Request $request)
    {
        $pxs = ToolPx::where('customer', $request->name)->get();
        foreach($pxs as $px) {
            $px->collection = 0;
            $px->remarks = 2;
            $px->update();
        }

        Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'px', 'amount'=>$request->amount]);
        return 1;
    }

    public function px_destroy(Request $request)
    {
        if($request->amount!=0) {
            $cash = Cash::where('tool','px')->where('created_at','like',$request->date.'%')->where('amount',$request->amount)->get();
            if($cash->count()) {
                ToolPx::where('id', $request->id)->delete();
                $cash->first()->delete();
                return 1;
            }
        }
        else {
            ToolPx::where('id', $request->id)->delete();
            return 1;
        }
        return 0;
    }

    public function px_install(Request $request)
    {
        $pxs = ToolPx::where('customer', $request->name)->where('remarks',1)->orderBy('amount')->get();
        $install = $request->install;
        Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'px', 'amount'=>$install]);
        foreach($pxs as $px) {
            if($install!=0) {
                if($install >= ($px->amount-$px->collection)) {
                    $install -= ($px->amount-$px->collection);
                    $px->collection = 0;
                    $px->remarks = 2;
                    $px->update();
                }
                else {
                    $px->collection = $px->collection+$install;
                    $install=0;
                    $px->update();
                }
            }
        }

        return 1;
    }
    /* END OF PHOTOCOPY/XEROX TOOL */


    /* AQUABYTE TOOL */
    public function aquabyte($type = 'Daily', $when = '2016')
    {
        $aqs = [];
        $aqs['overall_quantity'] = 0;
        $aqs['overall_amount'] = 0;
        $aqs['overall_collection'] = 0;
        $customer = '';
        foreach(ToolAq::where('created_at','like',$when.'%')->orderBy('remarks', 'desc')->orderBy('customer')->get() as $aq) {
            if($customer=='' || $customer!=$aq->customer) {
                $aqs[$aq->customer]['total_quantity'] = 0;
                $aqs[$aq->customer]['total_amount'] = 0;
                $aqs[$aq->customer]['total_collection'] = 0;
            }
            $customer = $aq->customer;

            if($aq->remarks == 1) {
                $aqs[$aq->customer]['total_quantity'] += $aq->quantity;
                $aqs[$aq->customer]['total_amount'] += $aq->amount;
                $aqs[$aq->customer]['total_collection'] += $aq->collection;
            }
            else {
                $aqs['overall_quantity'] += $aq->quantity;
                $aqs['overall_amount'] += $aq->amount;

            }
            $aqs['overall_collection'] += $aq->collection;

            $aqs[$aq->customer][] = [
                'id' => $aq->id,
                'transaction' => $aq->transaction_number,
                'quantity' => $aq->quantity,
                'amount' => $aq->amount,
                'remarks' => $aq->remarks,
                'collection' => $aq->collection,
                'created_at' => $aq->created_at,
            ];
        }
        return view('partials.cashier.aquabyte', ['role'=>Auth::user()->role, 'type'=>$type, 'when'=>$when, 'aqs'=>$aqs]);
    }

    public function aquabyte_store(Request $request)
    {
        $aq = new ToolAq($request->all());
        Auth::user()->toolaq()->save($aq);
        if($request->remarks == 0) {
            Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'aq', 'amount'=>$request->amount]); }
        if($request->remarks == 1&&($request->collection!=0 || $request->collection!='')) {
            Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'aq', 'amount'=>$request->collection]); }
        return 1;
    }

    public function aq_paycredit(Request $request)
    {
        $aq = ToolAq::find($request->id);
        $aq->collection = 0;
        $aq->remarks = 2;
        $aq->update();

        Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'aq', 'amount'=>$request->amount]);
        return 1;
    }

    public function aq_paybalance(Request $request)
    {
        $aqs = ToolAq::where('customer', $request->name)->get();
        foreach($aqs as $aq) {
            $aq->collection = 0;
            $aq->remarks = 2;
            $aq->update();
        }

        Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'aq', 'amount'=>$request->amount]);
        return 1;
    }

    public function aq_destroy(Request $request)
    {
        if($request->amount!=0) {
            $cash = Cash::where('tool','aq')->where('created_at','like',$request->date.'%')->where('amount',$request->amount)->get();
            if($cash->count()) {
                ToolAq::where('id', $request->id)->delete();
                $cash->first()->delete();
                return 1;
            }
        }
        else {
            ToolAq::where('id', $request->id)->delete();
            return 1;
        }
        return 0;
    }

    public function aq_install(Request $request)
    {
        $pxs = ToolAq::where('customer', $request->name)->where('remarks',1)->orderBy('amount')->get();
        $install = $request->install;
        Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'aq', 'amount'=>$install]);
        foreach($pxs as $px) {
            if($install!=0) {
                if($install >= ($px->amount-$px->collection)) {
                    $install -= ($px->amount-$px->collection);
                    $px->collection = 0;
                    $px->remarks = 2;
                    $px->update();
                }
                else {
                    $px->collection = $px->collection+$install;
                    $install=0;
                    $px->update();
                }
            }
        }

        return 1;
    }
    /* END OF AQUABYTE TOOL */

    /* E-LOAD TOOL */
    public function eload($type = 'Daily', $when = '2016')
    {
        $els = [];
        $numbers = ToolEl::groupBy('number')->lists('number')->toArray();
        $networks = ToolEl::groupBy('network')->lists('network')->toArray();

        $els['overall_load'] = 0;
        $els['overall_amount'] = 0;
        $els['overall_collection'] = 0;
        $customer = '';
        foreach(ToolEl::where('created_at','like',$when.'%')->orderBy('customer')->orderBy('remarks', 'desc')->orderBy('created_at', 'desc')->get() as $el) {
            if($customer=='' || $customer!=$el->customer) {
                $els[$el->customer]['total_load'] = 0;
                $els[$el->customer]['total_amount'] = 0;
                $els[$el->customer]['total_collection'] = 0;
            }
            $customer = $el->customer;

            if($el->remarks == 1) {
                //dd($el->load);
                $els[$el->customer]['total_load'] += $el->quantity;
                $els[$el->customer]['total_amount'] += $el->amount;
                $els[$el->customer]['total_collection'] += $el->collection;
            }
            else {
                $els['overall_load'] += $el->quantity;
                $els['overall_amount'] += $el->amount;
            }
            $els['overall_collection'] += $el->collection;

            $els[$el->customer][] = [
                'id' => $el->id,
                'transaction' => $el->transaction_number,
                'load' => $el->quantity,
                'network' => $el->network,
                'number' => $el->number,
                'amount' => $el->amount,
                'remarks' => $el->remarks,
                'collection' => $el->collection,
                'created_at' => $el->created_at,
            ];
        }
        return view('partials.cashier.e-load', ['role'=>Auth::user()->role, 'type'=>$type, 'when'=>$when, 'els'=>$els, 'numbers'=>$numbers, 'networks'=>$networks]);
    }

    public function eload_store(Request $request)
    {
        $el = new ToolEl($request->all());
        Auth::user()->toolel()->save($el);
        if($request->remarks == 0) {
            Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'el', 'amount'=>$request->amount]); }
        if($request->remarks == 1&&$request->collection!=0) {
            Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'el', 'amount'=>$request->collection]); }
        return 1;
    }

    public function el_paycredit(Request $request)
    {
        $el = ToolEl::find($request->id);
        $el->collection = 0;
        $el->remarks = 2;
        $el->update();

        Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'el', 'amount'=>$request->amount]);
        return 1;
    }

    public function el_paybalance(Request $request)
    {
        $els = ToolEl::where('customer', $request->name)->get();
        foreach($els as $el) {
            $el->collection = 0;
            $el->remarks = 2;
            $el->update();
        }

        Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'el', 'amount'=>$request->amount]);
        return 1;
    }

    public function el_destroy(Request $request)
    {
        if($request->amount!=0) {
            $cash = Cash::where('tool','el')->where('created_at','like',$request->date.'%')->where('amount',$request->amount)->get();
            if($cash->count()) {
                ToolEl::where('id', $request->id)->delete();
                $cash->first()->delete();
                return 1;
            }
        }
        else {
            ToolEl::where('id', $request->id)->delete();
            return 1;
        }

        return 0;
    }

    public function el_install(Request $request)
    {
        $pxs = ToolEl::where('customer', $request->name)->where('remarks',1)->orderBy('amount')->get();
        $install = $request->install;
        Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'el', 'amount'=>$install]);
        foreach($pxs as $px) {
            if($install!=0) {
                if($install >= ($px->amount-$px->collection)) {
                    $install -= ($px->amount-$px->collection);
                    $px->collection = 0;
                    $px->remarks = 2;
                    $px->update();
                }
                else {
                    $px->collection = $px->collection+$install;
                    $install=0;
                    $px->update();
                }
            }
        }

        return 1;
    }
    /* END OF E-LOAD TOOL */

    /* HANDS-ON TOOL */
    public function hands_on($type = 'Daily', $when = '2016')
    {
        $hos = [];
        $hos['overall_amount'] = 0;
        $hos['overall_collection'] = 0;
        $customer = '';
        foreach(ToolHo::where('time_in','like',$when.'%')->orderBy('customer')->orderBy('remarks', 'desc')->get() as $ho) {
            if($customer=='' || $customer!=$ho->customer) {
                $hos[$ho->customer]['total_amount'] = 0;
                $hos[$ho->customer]['total_collection'] = 0;
            }
            $customer = $ho->customer;

            if($ho->remarks == 1) {
                $hos[$ho->customer]['total_amount'] += $ho->amount;
                $hos[$ho->customer]['total_collection'] += $ho->collection;
            }
            else {
                $hos['overall_amount'] += $ho->amount;
            }
            $hos['overall_collection'] += $ho->collection;

            $hos[$ho->customer][] = [
                'id' => $ho->id,
                'transaction' => $ho->transaction_number,
                'time_in' => $ho->time_in,
                'time_out' => $ho->time_out,
                'amount' => $ho->amount,
                'remarks' => $ho->remarks,
                'collection' => $ho->collection,
            ];
        }
        return view('partials.cashier.hands-on', ['role'=>Auth::user()->role, 'type'=>$type, 'when'=>$when, 'hos'=>$hos]);
    }

    public function hands_on_store(Request $request)
    {
        $ho = new ToolHo($request->all());
        Auth::user()->toolho()->save($ho);
        if($request->remarks == 0) {
            Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'ho', 'amount'=>$request->amount]); }
        if($request->remarks == 1&&($request->collection!=0 || $request->collection!='')) {
            Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'ho', 'amount'=>$request->collection]); }
        return $ho;
    }

    public function ho_paycredit(Request $request)
    {
        $ho = ToolHo::find($request->id);
        $ho->collection = 0;
        $ho->remarks = 2;
        $ho->update();

        Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'ho', 'amount'=>$request->amount]);
        return 1;
    }

    public function ho_paybalance(Request $request)
    {
        $hos = ToolHo::where('customer', $request->name)->get();
        foreach($hos as $ho) {
            $ho->collection = 0;
            $ho->remarks = 2;
            $ho->update();
        }

        Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'ho', 'amount'=>$request->amount]);
        return 1;
    }

    public function ho_destroy(Request $request)
    {
        if($request->amount!=0) {
            $cash = Cash::where('tool','ho')->where('created_at','like',$request->date.'%')->where('amount',$request->amount)->get();
            if($cash->count()) {
                ToolHo::where('id', $request->id)->delete();
                $cash->first()->delete();
                return 1;
            }
        }
        else {
            ToolHo::where('id', $request->id)->delete();
            return 1;
        }
        return 0;
    }

    public function ho_install(Request $request)
    {
        $pxs = ToolHo::where('customer', $request->name)->where('remarks',1)->orderBy('amount')->get();
        $install = $request->install;
        Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'ho', 'amount'=>$install]);
        foreach($pxs as $px) {
            if($install!=0) {
                if($install >= ($px->amount-$px->collection)) {
                    $install -= ($px->amount-$px->collection);
                    $px->collection = 0;
                    $px->remarks = 2;
                    $px->update();
                }
                else {
                    $px->collection = $px->collection+$install;
                    $install=0;
                    $px->update();
                }
            }
        }

        return 1;
    }
    /* END OF HANDS-ON TOOL */

    /* SERVICE TOOL */
    public function service($type = 'Daily', $when = '2016')
    {
        $pxs = [];
        $pxs['overall_amount'] = 0;
        $pxs['overall_collection'] = 0;
        $customer = '';
        foreach(ToolSe::where('created_at','like',$when.'%')->orderBy('customer')->orderBy('remarks', 'desc')->orderBy('created_at', 'desc')->get() as $px) {
            if($customer=='' || $customer!=$px->customer) {
                $pxs[$px->customer]['total_amount'] = 0;
                $pxs[$px->customer]['total_collection'] = 0;
            }
            $customer = $px->customer;

            if($px->remarks == 1) {
                $pxs[$px->customer]['total_amount'] += $px->amount;
                $pxs[$px->customer]['total_collection'] += $px->collection;
            }
            else {
                $pxs['overall_amount'] += $px->amount;
            }
            $pxs['overall_collection'] += $px->collection;

            $pxs[$px->customer][] = [
                'id' => $px->id,
                'transaction' => $px->transaction_number,
                'desc' => $px->desc,
                'amount' => $px->amount,
                'remarks' => $px->remarks,
                'collection' => $px->collection,
                'created_at' => $px->created_at,
            ];
        }
        return view('partials.cashier.service', ['role'=>Auth::user()->role, 'type'=>$type, 'when'=>$when, 'pxs'=>$pxs]);
    }

    public function service_store(Request $request)
    {
        $px = new ToolSe($request->all());
        Auth::user()->toolpx()->save($px);
        if($request->remarks == 0) {
            Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'se', 'amount'=>$request->amount]); }
        if($request->remarks == 1&&($request->collection!=0 || $request->collection!='')) {
            Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'se', 'amount'=>$request->collection]); }
        return 1;
    }

    public function se_paycredit(Request $request)
    {
        $px = ToolSe::find($request->id);
        $px->collection = 0;
        $px->remarks = 2;
        $px->update();

        Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'se', 'amount'=>$request->amount]);
        return 1;
    }

    public function se_paybalance(Request $request)
    {
        $pxs = ToolSe::where('customer', $request->name)->get();
        foreach($pxs as $px) {
            $px->collection = 0;
            $px->remarks = 2;
            $px->update();
        }

        Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'se', 'amount'=>$request->amount]);
        return 1;
    }

    public function se_destroy(Request $request)
    {
        if($request->amount!=0) {
            $cash = Cash::where('tool','se')->where('created_at','like',$request->date.'%')->where('amount',$request->amount)->get();
            if($cash->count()) {
                ToolSe::where('id', $request->id)->delete();
                $cash->first()->delete();
                return 1;
            }
        }
        else {
            ToolSe::where('id', $request->id)->delete();
            return 1;
        }
        return 0;
    }

    public function se_install(Request $request)
    {
        $pxs = ToolSe::where('customer', $request->name)->where('remarks',1)->orderBy('amount')->get();
        $install = $request->install;
        Cash::create(['user_id'=>Auth::user()->id, 'tool'=>'se', 'amount'=>$install]);
        foreach($pxs as $px) {
            if($install!=0) {
                if($install >= ($px->amount-$px->collection)) {
                    $install -= ($px->amount-$px->collection);
                    $px->collection = 0;
                    $px->remarks = 2;
                    $px->update();
                }
                else {
                    $px->collection = $px->collection+$install;
                    $install=0;
                    $px->update();
                }
            }
        }

        return 1;
    }
    /* END OF SERVICE TOOL */

    public function convertTime(Request $request)
    {
        $tin = Carbon::parse($request->tin);
        $tout = Carbon::parse($request->tout);
        return $tout->diffInMinutes($tin);
    }

}
