<?php

namespace App\Http\Controllers;

use Auth;
use App\Log;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LogController extends Controller
{
    protected $style = [
        1 => ['color'=>'blue','icon'=>'key','title'=>'User has login successfully'],
        2 => ['color'=>'maroon','icon'=>'wrench','title'=>'Cashier sale tool was opened'],
        3 => ['color'=>'yellow','icon'=>'print','title'=>'Sales report was printed'],
        4 => ['color'=>'purple','icon'=>'star','title'=>'New product was created'],
        5 => ['color'=>'aqua','icon'=>'save','title'=>'Item updated'],
        6 => ['color'=>'maroon','icon'=>'eraser','title'=>'Item was removed'],
        7 => ['color'=>'yellow','icon'=>'user','title'=>'New cashier was created'],
        8 => ['color'=>'blue','icon'=>'user','title'=>'Cashier profile was updated'],
        9 => ['color'=>'maroon','icon'=>'remove','title'=>'Cashier profile was deleted successfully'],
        10 => ['color'=>'yellow','icon'=>'user','title'=>'User account was updated'],
        11 => ['color'=>'aqua','icon'=>'star','title'=>'Searching transaction #'],
        12 => ['color'=>'blue','icon'=>'check','title'=>'Confirm payment of transaction #'],
        13 => ['color'=>'blue','icon'=>'check','title'=>'Balance has been paid for ___ credit account'],
        14 => ['color'=>'aqua','icon'=>'check','title'=>'Install ___ pesos to credit. Transation #'],
    ];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $user = Auth::user();

        if(isset($_GET['access'])) {
            $userlogs = Log::where('user_id',$_GET['access'])->orderBy('created_at','DESC')->get();
        }
        else {
            $userlogs = Log::where('user_id',$user->id)->orderBy('created_at','DESC')->where('created_at', '>', Carbon::now()->subDays(5))->get();
        }

        $logs=[];
        foreach($userlogs as $log) {
            $key = $log->created_at->year.$log->created_at->month.$log->created_at->day;
            $logs[$key][] = [
                'type' => $log->type,
                'title' => $log->title,
                'description' => $log->description,
                'created_at' => $log->created_at,
            ];
        }
        //dd($user->toArray());
        return view('partials.timeline', ['role'=>$user->role, 'logs'=>$logs, 'style'=>$this->style]);
    }

    public function ajaxLog($type)
    {
        return Log::create(['user_id'=>Auth::user()->id, 'type'=>$type, 'title'=>$this->style[$type]['title']]);
    }
}
