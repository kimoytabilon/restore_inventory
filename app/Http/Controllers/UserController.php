<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Log;
use Carbon\Carbon;
use Hash;
use Input;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $role = Auth::user()->role;
        if($role == 'admin') {
            $cashiers = User::where('role','!=','admin')->orderBy('role','DESC')->get()->toArray();
        }
        elseif($role == 'owner') {
            $cashiers = User::where('role','!=','admin')->where('role','!=','owner')->orderBy('role','DESC')->get()->toArray();
        } 
        else {
            $cashiers = User::where('role','cashier')->orderBy('name')->get()->toArray();
        }

        return view('partials.manage-user', ['cashiers'=>$cashiers,'role'=>$role]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $active = Auth::user();
        $user = new User;

        $user->name = $request->name;
        $user->img = 'user2-160x160.jpg';
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        if($active->role == 'admin') {
            $user->role = $request->role;
        }
        else {
            $user->role = 'cashier';
        }

        $user->save();

        Log::create(['user_id'=>$active->id, 'type'=>7, 'title'=>'New user was created : #'.$user->name]);

        return redirect($active->role.'/manage-user');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $user->name = $request->name;
        $user->email = $request->email;
        if($request->password) {
            $user->password = bcrypt($request->password);
        }
        $user->update();
        Log::create(['user_id'=>Auth::user()->id, 'type'=>8, 'title'=>$request->name.' profile was updated']);
        return 1;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        Log::create(['user_id'=>Auth::user()->id, 'type'=>9, 'title'=>$user->name.' profile was deleted successfully']);

        $user->delete();

        return 1;
    }

    public function account()
    {
        $user = Auth::user();
        return view('partials.edit-account',['role'=>$user->role,'info'=>User::find($user->id)]);
    }

    public function account_update(Request $request)
    {
        $user = User::find(Auth::user()->id);

        if(Hash::check($request->old_password,$user->password)) {
            if($request->file('img')) {
                $imageName = Auth::user()->name . '.' . $request->file('img')->getClientOriginalExtension();
                $user->img = $imageName;
                $request->file('img')->move('assets\dist\img', $imageName);
            }
            $user->email = $request->email;
            $user->password = bcrypt($request->new_password);
            $user->update();
            Log::create(['user_id'=>Auth::user()->id, 'type'=>10, 'title'=>'User account was updated']);


            return 1;
        }
        return 0;
    }

    public function lockscreen()
    {
        if(Auth::check()) {
            $profile = Auth::user();
            Auth::logout();
            return view('auth.lockscreen')
                ->with('profile',$profile);
        }
        return redirect(url());

    }

    public function upload() {
        $file = Input::file('image');
        $input = array('image' => $file);
        $rules = array(
            'image' => 'image'
        );
        $validator = Validator::make($input, $rules);
        if ( $validator->fails() )
        {
            return Response::json(['success' => false, 'errors' => $validator->getMessageBag()->toArray()]);

        }
        else {
            $destinationPath = 'uploads/';
            $filename = $file->getClientOriginalName();
            Input::file('image')->move($destinationPath, $filename);
            return Response::json(['success' => true, 'file' => asset($destinationPath.$filename)]);
        }

    }
}
