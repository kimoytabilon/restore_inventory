<?php

namespace App\Http\Controllers;

use App\Sale;
use App\ToolAq;
use App\ToolEl;
use App\ToolHo;
use App\ToolPx;
use App\ToolSe;
use Auth;
use App\Product;
use App\Log;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ProductRequest;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $products=[];
        $names = [];
        foreach(Product::orderBy('name')->orderBy('description')->get() as $product)
        {
            $products[$product->name][] = [
                'id'=>$product->id,
                'code'=>$product->code,
                'description'=>$product->description,
                'quantity'=>$product->quantity,
                'unit'=>$product->unit,
                'original_price'=>$product->original_price,
                'cash_price'=>$product->cash_price,
                'credit_price'=>$product->credit_price,
            ];
        }

        foreach($products as $productName => $productInfo)
        {
            $names[] = $productName;
        }

        return view('partials.owner.product', ['names'=>$names,'products'=>$products, 'profile'=>Auth::user()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ProductRequest  $request
     * @return Response
     */
    public function store(ProductRequest $request)
    {
        if($request->code=='' || !Product::where('code',$request->code)->count()) {
            $product = new Product($request->all());
            Auth::user()->products()->save($product);
            Log::create(['user_id'=>Auth::user()->id, 'type'=>4, 'title'=>'New product was created : #'.$request->description]);
            return back();
        }
        return back()->withInput()->with('error','Duplicate barcode!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ProductRequest  $request
     * @param  Product $product
     * @return Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        $product->update($request->all());
        Log::create(['user_id'=>Auth::user()->id, 'type'=>5, 'title'=>'Item updated : #'.$request->description]);
        return 1;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Product $product
     * @return Response
     */
    public function destroy(Product $product)
    {
        Log::create(['user_id'=>Auth::user()->id, 'type'=>6, 'title'=>'Item was removed : #'.$product->description]);
        if($product->delete())
        {
            return 1;
        }
        return 0;
    }

    public function productToJson($barcode = false)
    {
        $products = [];

        if ($barcode) {
            //return Product::where('code', $barcode)->get()->toJson();
            $list = Product::where('quantity','>',0)->where('code', $barcode)->get();
        }
        else {
            $list = Product::where('quantity','>',0)->get();
        }

        foreach ($list as $product) {
            $products[] = [
                'value' => $product->description,
                'label' => $product->name,
                'id' => $product->id,
                'code' => $product->code,
                'quantity' => $product->quantity,
                'unit' => $product->unit,
                'original_price' => $product->original_price,
                'cash_price' => $product->cash_price,
                'credit_price' => $product->credit_price,
            ];
        }

        return json_encode($products);
    }

    public function inquireItems()
    {
        $products = [];
        $list = Product::get();
        foreach ($list as $product) {
            $products[] = [
                'value' => $product->description,
                'label' => $product->name.' Cash:(' .$product->cash_price.'), Credit:('.$product->credit_price.'), Left:('.$product->quantity.') '.$product->unit.'/s',
            ];
        }

        return json_encode($products);
    }

    public function creditNamesToJson()
    {
        $credit_names = [
            'sa' => Sale::groupBy('credit_info')->get(['credit_info as customer']),
            'aq' => ToolAq::groupBy('customer')->get(['customer']),
            'el' => ToolEl::groupBy('customer')->get(['customer']),
            'ho' => ToolHo::groupBy('customer')->get(['customer']),
            'px' => ToolPx::groupBy('customer')->get(['customer']),
            'se' => ToolSe::groupBy('customer')->get(['customer']),
        ];

        foreach ($credit_names as $names) {
            foreach ($names as $name) {
                $list[$name->customer] = '';
            }
        }

        foreach ($list as $name=>$ex) {
            $credit_name[] = [
                'value' => $name,
            ];
        }

        return json_encode($credit_name );
    }
}
