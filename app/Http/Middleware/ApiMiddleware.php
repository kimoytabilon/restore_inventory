<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class ApiMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::user())
        {
            return response('Unauthorized.', 401);
        }
        return $next($request);
    }
}
