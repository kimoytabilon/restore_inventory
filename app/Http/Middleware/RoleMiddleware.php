<?php

namespace App\Http\Middleware;

use Closure;
use App\Log;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if($request->user()->role != $role) {
            Log::create([
                'user_id'=>$request->user()->id,
                'type'=>1,
                'title'=>'User has login successfully',
                'description'=>'',
            ]);
            return redirect('/'.$request->user()->role.'/dashboard');
        }

        return $next($request);
    }
}
