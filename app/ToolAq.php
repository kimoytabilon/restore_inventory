<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ToolAq extends Model
{
    protected $table = 'tool_aquabyte';

    protected $fillable = [
        'user_id',
        'transaction_number',
        'customer',
        'amount',
        'quantity',
        'price',
        'discount',
        'remarks',
        'collection',
        'created_at',
    ];
}
