<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ToolEl extends Model
{
    protected $table = 'tool_eload';

    protected $fillable = [
        'user_id',
        'transaction_number',
        'network',
        'customer',
        'number',
        'quantity',
        'amount',
        'price',
        'discount',
        'remarks',
        'collection',
        'created_at',
    ];
}
