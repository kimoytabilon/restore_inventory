<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
		'code',
		'name',
		'description',
		'quantity',
		'unit',
		'original_price',
		'cash_price',
		'credit_price',
		'created_by',
		'updated_by',
	];

	protected $hidden = [];

	/**
	 * User Product
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
	public function user()
	{
		return $this->belongsTo('App\User', 'created_by');
	}

	public function sale()
	{
		return $this->hasMany('App\Sale', 'product_id');
	}
}
