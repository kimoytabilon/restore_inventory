<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Settings
    |--------------------------------------------------------------------------
    |
    */
    'version'     => '1.0',
    'company'     => 'Kiakaha',
    'website'     => 'http://www.kiakaha.co',
    'name'        => 'Inventory & POS',
    'shortname'   => '.IPOS',
    'role'        => [ 'owner', 'manager', 'cashier' ],
    'lockscreen'  => '20',
];    